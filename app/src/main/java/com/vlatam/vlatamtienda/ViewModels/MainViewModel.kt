package com.vlatam.vlatamtienda.ViewModels

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Environment
import android.os.Looper
import android.util.Log
import com.vlatam.vlatamtienda.Helper.Resource
import com.google.android.gms.location.*
import com.google.gson.Gson
import com.itextpdf.text.*
import com.itextpdf.text.pdf.PdfWriter
import com.vlatam.vlatamtienda.Data.Local.*
import com.vlatam.vlatamtienda.Data.Repository.*
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.Servicios.PrefrencesManager
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import com.vlatam.vlatmrest.Helper.Utils
import okhttp3.*
import org.json.JSONObject
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import java.io.ByteArrayOutputStream
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.util.Base64
import com.google.firebase.iid.FirebaseInstanceId
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.vlatam.vlatamtienda.Servicios.ServicioDescarga
import java.text.DecimalFormat


class MainViewModel constructor(val preferences: PrefrencesManager): ViewModel() {
    private val client= OkHttpClient()
    private val context= VlatamTiendaApplication.instance.getContext()

    private val daoPedidos = PedidoDao()
    private val daoClientes = ClienteDao()
    private val daoArticulos= ArticulosDao()
    private val daoDirectorio= DirectorioDao()
    private val daoTarifas= TarifasDao()
    private val daoPrecios= PreciosDao()
    private val daoPedidoArticulo= PedidoArticuloDao()
    private val daoTarifaCliente= TarifaClienteDao()
    private val daoMoneda= MonedaDao()
    private val intent = Intent(context, ServicioDescarga::class.java)

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private lateinit var mLocationRequest: LocationRequest
    private lateinit var mLocationSettingsRequest: LocationSettingsRequest

    private var location: Location?= null
    private var create_pedido: Boolean= false
    private var create_pedido_qr: Boolean= false

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            // location is received
            location = locationResult!!.lastLocation
            if(create_pedido){
                create_pedido= false
                createNewPedido()
            }else
                if(create_pedido_qr){
                    create_pedido_qr= false
                    nuevoPedidoQr(cliente_qr)
                }
        }
    }

//,
    var facturas= MutableLiveData<Resource<ClienteFacturas>>()
    var user_data= MutableLiveData<Resource<HashMap<String, String>>>()
    val pedidos = MutableLiveData<Resource<MutableList<Pedido>>> () /**  lista de pedidos del menu drawer **/
    var searchList= MutableLiveData<Resource<MutableList<SearchResponse>>> () /** Lista de items del search view**/
    var pedido= MutableLiveData<Resource<Pedido>>()
    val list_clientes = MutableLiveData<Resource<List<EntM>>> ()

    var artMList= MutableLiveData<Resource<MutableList<PedidoArticulo>>> () /**  lista de articulos de un pedido_selected **/
    val cliente = MutableLiveData<Resource<ClienteDireccion>> ()  /** cliente de un pedido_selected **/


    var pedido_selected= Pedido()/** cliente de un pedido_selected **/
    var ent_m: ClienteDireccion? = null  /** cliente de un pedido_selected **/
    var pedido_articulo_list: MutableList<PedidoArticulo> = arrayListOf()

    var list_pedidos: MutableList<Pedido> = arrayListOf()

    private var contador=0
    //private var CONT=0


    /** para compartir la factura */
    val file_d= MutableLiveData<Resource<File>> ()
    val bfBold12: Font = Font(Font.FontFamily.TIMES_ROMAN, 12f, Font.BOLD, BaseColor(0, 0, 0))
    val bf12 = Font(Font.FontFamily.TIMES_ROMAN, 12f)
    lateinit var file: File
    lateinit var document: Document


    fun checkDatosInicio(){
        if(preferences.isLogged()){
            val mMap = HashMap<String, String>()
            mMap["nom_com"] = preferences.getEntM().nom_com
            mMap["img"] = preferences.getEntM().img
            Log.e("TAG", "NOMBRE ${preferences.getEntM().nom_com}   ${preferences.getEntM().img} ")
            user_data.postValue(Resource.success(mMap))

        }else{
            user_data.postValue(Resource.error("",null))
            return
        }


        pedidos.value= Resource.loading(arrayListOf())

        if(daoArticulos.queryForAll().size==0){
            descargarDatos()
        }else{
            enviarToken()
            updateListPedido()
        }
    }


    fun descargarDatos(){
        context.startService(intent)
    }

    fun stopServicio() {
        context.stopService(intent)
    }

    fun cerrarSesion() {
        preferences.logout()
        daoPedidos.removeAll()
        daoPedidoArticulo.removeAll()
        daoDirectorio.removeAll()
        daoClientes.removeAll()
        daoArticulos.removeAll()
        daoTarifas.removeAll()
        daoPedidos.removeAll()

        user_data.postValue(Resource.error("",null))
    }


    fun reload(){
        descargarDatos()
        //consumeEnt_m()
        //consumeDir_m()


        //consumeArt_m(1)
        //consumeTarifas()
        //consumePrecios()
    }


    private var lista_art=daoArticulos.queryForAll()

    fun doSearch(text: String, limit: Int = 5){
        /*if(text.isEmpty()){
            searchList.value= Resource.success(arrayListOf())
            artMList.value=Resource.success(pedido_articulo_list)
            if(ent_m!=null)
                cliente.value= Resource.success(ent_m)
            return
        }*/
        Log.e("TAG", "BUSCANDO ESTE TEXTO $text")
        var data : MutableList<SearchResponse> = arrayListOf()
        //searchList.postValue(Resource.loading(data))


        if(text.isEmpty()) {
            lista_art.forEach { it ->
                data.add(SearchResponse(it.name, it))
            }
        }else {
            lista_art.forEach { it ->
                if (it.name.toLowerCase().contains(text.toLowerCase())) {
                    data.add(SearchResponse(it.name, it))
                }
            }
        }

        /*daoClientes.queryForAll().forEach {it ->
            if(it.nom_com.toLowerCase().contains(text.toLowerCase())) {
                //consulte las direcciones
                var direcciones= daoDirectorio.queryForAll(it.id!!)

                if(direcciones.size>0){
                    for (dir in direcciones) {
                        data.add(SearchResponse(it.nom_com, ClienteDireccion(it, dir)))
                    }
                }else
                    data.add(SearchResponse(it.nom_com, ClienteDireccion(it, null)))
            }
        }*/


        if(!data.isEmpty())
            data= data.sortedBy{ it.name} as MutableList<SearchResponse>

        //Log.e("TAG", "actualizanod "+data.size)
        searchList.value= Resource.success(data)
        /*if(data.size>limit)
            searchList.value=Resource.success(data.subList(0, limit))
        else
            searchList.value=Resource.success(data)*/

    }

    private var lista_client = daoClientes.queryForAll()

    fun doSearchClient(text: String, limit: Int = 5){
        /*if(text.isEmpty()){
            searchList.value= Resource.success(arrayListOf())
            artMList.value=Resource.success(pedido_articulo_list)
            if(ent_m!=null)
                cliente.value= Resource.success(ent_m)
            return
        }*/
        Log.e("TAG", "BUSCANDO ESTE TEXTO CLIENTE $text")

        var data : MutableList<SearchResponse> = arrayListOf()
        //searchList.postValue(Resource.loading(data))


        if(text.isEmpty()){

            lista_client.forEach {it ->
                //consulte las direcciones
                var direcciones= daoDirectorio.queryForAll(it.id!!)

                if(direcciones.size>0){
                    for (dir in direcciones) {
                        data.add(SearchResponse(it.nom_com, ClienteDireccion(it, dir)))
                    }
                }else
                    data.add(SearchResponse(it.nom_com, ClienteDireccion(it, null)))
            }

        }else{
            lista_client.forEach {it ->
                if(it.nom_com.toLowerCase().contains(text.toLowerCase())) {
                    //consulte las direcciones
                    var direcciones= daoDirectorio.queryForAll(it.id!!)

                    if(direcciones.size>0){
                        for (dir in direcciones) {
                            data.add(SearchResponse(it.nom_com, ClienteDireccion(it, dir)))
                        }
                    }else
                        data.add(SearchResponse(it.nom_com, ClienteDireccion(it, null)))
                }
            }
        }


        //Log.e("TAG", "actualizanod "+data.size)
        if(!data.isEmpty())
            data= data.sortedBy{ it.name} as MutableList<SearchResponse>


        searchList.value= Resource.success(data)

        /*if(data.size>limit)
            searchList.value=Resource.success(data.subList(0, limit))
        else
            searchList.value=Resource.success(data)*/

    }

    @SuppressLint("MissingPermission")
    fun prepareLocation(){

        Log.e("TAG", "PREPARE LOCATION ")
        if(!Utils.isGPSProvider(context)){
            cliente.value= Resource.error("2", null)
            return
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(VlatamTiendaApplication.instance.getContext())
        mLocationRequest = LocationRequest()

        mLocationRequest.interval = 30
        mLocationRequest.fastestInterval = 30
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
        builder.setAlwaysShow(false)
        builder.addLocationRequest(mLocationRequest)
        mLocationSettingsRequest = builder.build()


        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper())
    }


    fun add(it: Any) {
        //anten de agregar cualquier producto oc liente comprobar que esta activo el gps para la hora de guaradar
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            cliente.value= Resource.error("3", null)
            return
        }

        if(pedido_selected.is_sincronizado){
            pedido.value= Resource.error("2", null)
            return
        }


        if(!Utils.isGPSProvider(context)){
            cliente.value= Resource.error("2", null)
            return
        }


        Log.e("TAG", "articulo seleccionado ${Gson().toJson(it)}")

        if (it is ArtM) {
            var pedido_articulo= PedidoArticulo()
            pedido_articulo.art_m= calculatePrecio(it)

            if(!pedido_articulo_list.contains(pedido_articulo)) {
                pedido_articulo_list.add(pedido_articulo)
                updateTotalPedido()
                artMList.value = Resource.success(pedido_articulo_list)
                if(!pedido_selected.is_editable){
                    pedido_articulo.pedido= pedido_selected
                    daoPedidoArticulo.add(pedido_articulo)
                }
            }
        }else
            if (it is ClienteDireccion) {
                ent_m= it
                ent_m!!.vta_tar_g= daoTarifas.queryForAll()
                Log.e("TAG", "cliente direccion seleccionado ${ent_m!!.vta_tar_g}")

                if(!pedido_selected.is_editable){
                    pedido_selected.cliente= ent_m!!.cliente

                    daoPedidos.update(pedido_selected)
                    pedido.value= Resource.success(pedido_selected)
                }


                //Log.e("TAG", "vta_tar_g ${pedido_selected.vta_tar_g}")

                cliente.value= Resource.success(ent_m)



            }

    }



    private fun calculatePrecio(it: ArtM): ArtM{
        /** CALCULANDO EL PRECIO DEL PRODUCTO */
        if(daoTarifas.queryForAll().isEmpty()){
            var tarifa= daoTarifaCliente.queryTarifaCliente(ent_m!!.cliente.id!!, it.id!!)
            if(tarifa!=null){
                it.pvp= tarifa.pre
                Log.e("TAG", "NUEVO PRECIO DEL ARTICULO $it")
            }
        }else {

            var tarifa_2= daoPrecios.queryTarifa(pedido_selected.vta_tar, it.id!!)

            if(tarifa_2!=null) {
                it.pvp = tarifa_2.pre
                Log.e("TAG", "NUEVO PRECIO DEL ARTICULO con vta_tar $it")
            }
        }


        return it
    }


    fun selectedVtaTar(it: VtaTarG) {
        Log.e("TAG", "selectedVtaTar $it")
        try {
            pedido_selected.vta_tar=  it.id!!
            ent_m!!.cliente.vta_tar= it.id

            pedido_articulo_list.forEach {
                it.art_m= calculatePrecio(daoArticulos.queryForId(it.art_m!!.id!!))
            }

            updateTotalPedido()
            Log.e("TAG", "LISTA DE ARTICULOS "+Gson().toJson(pedido_articulo_list))

            artMList.value= Resource.success(pedido_articulo_list)

        }catch (e: Exception){
            Log.e("TAG", "error selectedVtaTar ${e.message}")

        }

    }



    @SuppressLint("MissingPermission")
    fun createNewPedido() {

        /** COMPRUEBA QUE TIENE CLIENTE ASIGNADO Y QUE TIENE AL MENOS UN ARTIULO */
        if(ent_m==null){
            cliente.value= Resource.error("1", null)
            return
        }

        if(location!=null){
            Log.e("TAG", "getLastLocation "+location!!.latitude+"  "+ location!!.longitude)

            if(pedido_selected.is_editable) {

                pedido_selected.is_editable = false
                pedido_selected.cliente = ent_m!!.cliente
                if (ent_m!!.direccion != null)
                    pedido_selected.direccion = ent_m!!.direccion


                pedido_selected.lat = location!!.latitude
                pedido_selected.lon = location!!.longitude
                pedido_selected.alt = location!!.altitude
                pedido_selected.pre = location!!.accuracy
                pedido_selected.vel = location!!.speed

                //pedido_selected.vta_tar=  pedido_selected.cliente!!.vta_tar


                pedido_selected.hora=  SimpleDateFormat("dd/MM/yyyy hh:MM:ss").format(Date())

                Log.e("TAG", "PEDIDO A GUARDAR $pedido_selected")

                daoPedidos.add(pedido_selected)

                var aux = daoPedidos.getUltimoPedido()
                Log.e("TAG", "PEDIDO GUARDADO $aux")

                for (pedidoArticulo in pedido_articulo_list) {
                    pedidoArticulo.pedido = aux
                    Log.e("TAG", "artiuclos $pedidoArticulo")
                    daoPedidoArticulo.add(pedidoArticulo)
                }

                updateListPedido()

                nuevoPedido()


            }
        } else {
            Log.e("TAG", "getLastLocation:exception ")
            pedido.postValue(Resource.error("3", null))
            create_pedido=true

            prepareLocation()

        }






    }

    fun nuevoPedido() {

        ent_m = null
        pedido_selected = Pedido()
        pedido_articulo_list.clear()

        cliente.postValue(Resource.error("4", null))
        artMList.postValue(Resource.error("4", null))
        pedido.postValue(Resource.success(pedido_selected))
        pedido.postValue(Resource.error("4", null)) //cambiar switch para buscar cliente nuevamente

    }



    fun update(it: PedidoArticulo) {

        for(num in 1..pedido_articulo_list.size) {

            var pedidoArticulo= pedido_articulo_list[num-1]

            if(pedidoArticulo == it){
                if(it.cantidad==0f){
                    pedido_articulo_list.remove(it)
                    daoPedidoArticulo.delete(it)
                }else {
                    pedidoArticulo.cantidad = it.cantidad
                    daoPedidoArticulo.update(pedidoArticulo)
                    /** ACTUALIZAR EL TOTAL DEL PEDIDO */
                    Log.e("TAG", "LISTA articulos $pedido_articulo_list")
                }
                updateTotalPedido()
                return
            }

        }
        /*
        for (pedidoArticulo in pedido_articulo_list) {



        }*/
    }

    private fun updateTotalPedido() {
        var total_cantidad= 0f
        var total= 0f

        for (pedidoArticulo in pedido_articulo_list) {
            total_cantidad+=pedidoArticulo.cantidad
            var t=calculatePrecio(pedidoArticulo.art_m!!).pvp!! * pedidoArticulo.cantidad
            total+= t
        }

        pedido_selected.total= total
        pedido_selected.total_articulos= total_cantidad
        pedido_selected.mon_sim= preferences.getEmpM().mon_sim

        if(!pedido_selected.is_editable)
            daoPedidos.update(pedido_selected)

        pedido.value= Resource.success(pedido_selected)

    }


    fun updateListPedido() {

        list_pedidos= daoPedidos.queryForAllPendientes()

        if(!list_pedidos.isEmpty()){
            list_pedidos= list_pedidos.sortedByDescending { it.id } as MutableList<Pedido>

            list_pedidos.forEach{ x ->
                x.cliente= daoClientes.queryForId(x.cliente!!.id!!)
            }

            pedidos.postValue(Resource.success(list_pedidos))
        }else{
            pedidos.postValue(Resource.success(list_pedidos))
        }

    }

    fun selectedPedido(p: Pedido) {
        pedido_selected=p
        Log.e("TAG", "pedido seleccionado $pedido_selected")

        if(pedido_selected.direccion!=null){
            var dir= daoDirectorio.queryForId(pedido_selected.direccion!!.id!!)
            pedido_selected.direccion= dir
        }

        if(pedido_selected.cliente!=null){
            var clien= daoClientes.queryForId(pedido_selected.cliente!!.id!!)
            pedido_selected.cliente= clien
        }

        //ent_m= ClienteDireccion(clien!!, dir)
        ent_m= ClienteDireccion(pedido_selected.cliente!!, pedido_selected.direccion)
        pedido_articulo_list.clear()
        pedido_articulo_list= daoPedidoArticulo.queryForAll(pedido_selected.id!!)

        pedido_articulo_list.forEach {
            it.art_m= calculatePrecio(daoArticulos.queryForId(it.art_m!!.id!!))
        }

        Log.e("TAG", "LISTA DE ARTICULOS "+Gson().toJson(pedido_articulo_list))



        //para la interfaz
        artMList.value= Resource.success(pedido_articulo_list)
        pedido.value= Resource.success(pedido_selected)
        cliente.value= Resource.success(ent_m)
    }



    fun sincronizar(p: Pedido){
        pedido_selected.obs= p.obs
        daoPedidos.update(pedido_selected)

        if(pedido_selected.direccion!=null)
            pedido_selected.direccion= daoDirectorio.queryForId(pedido_selected.direccion!!.id!!)

        pedido_selected.cliente= daoClientes.queryForId(pedido_selected.cliente!!.id!!)



        Log.e("TAG", "PEDIDO ACTUALIZADO $pedido_selected")
        enviarCabecera()

    }
    fun getListClientes() {
        list_clientes.postValue(Resource.success(daoClientes.queryForAll()))
    }


    fun selectedCliente(ent: EntM) {
        //https://tienda.vlatamweb.com/API/vLatamERP_db_dat/v1/cta_cor_t?filter[ent_tip_cta_cor_pdt]=2,F&api_key=tienda123

        var endpoint= preferences.getBasrUrl()+"v1/cta_cor_t?filter[ent_tip_cta_cor_pdt]=${ent.id},F&api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                //message.postValue(context.getString(R.string.error_api)+" ${e.message}")
                pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO facturas $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
//                    message.postValue("El API Key de la solicitud no es válido")
                    pedidos.postValue(Resource.error("El API Key de la solicitud no es válido", arrayListOf()))

                }else
                    if (response.isEmpty() || response == "{}") {
                        //message.postValue(context.getString(R.string.error_api))
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))

                    } else {

                        try {
                            var data = Gson().fromJson(response, ResponseFacturas::class.java)

                            var lista= data.cta_cor_t

                            lista.forEach { fc->
                                fc.moneda= daoMoneda.queryForId(fc.mon_c).sim
                            }

                            facturas.postValue(Resource.success(ClienteFacturas(ent, lista)))

                        } catch (e: Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))

                        }

                    }
            }
        })

    }

    @Synchronized fun synchronizedMethod() {
        println("inside a synchronized method: ${Thread.currentThread()}")
    }
    private fun enviarLinea(artM: PedidoArticulo, pos: Int, cabecera: VtaPedG){

        var endpoint= preferences.getBasrUrl()+"v1/vta_ped_lin_g?api_key="+ (preferences.getLoginInfo()?.api_key ?: "")

        var date= SimpleDateFormat("yyyy-MM-dd")
        var hora= SimpleDateFormat("HH:mm:ss")

        var fecha= SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(pedido_selected.hora)

        Log.e("TAG", "FECHA A ENVIAR en linea $fecha")

        var param = JSONObject()
        param.put("id", 0)
        param.put("vta_ped", cabecera.id)
        param.put("num_lin", pos)
        //param.put("fch", date.format(fecha)+"T"+hora.format(fecha)+".000Z")
        //param.put("hor", hora.format(fecha))
        param.put("fch", date.format(fecha))
        param.put("hor", hora.format(fecha))
        param.put("emp", cabecera.emp_div) /***/
        param.put("clt", cabecera.clt) /***/
        param.put("ser", cabecera.ser)
        param.put("alm", cabecera.alm)
        param.put("art", artM.art_m!!.id)
        param.put("dsc", "")
        param.put("can_ped", artM.cantidad)
        param.put("can_ped_und", artM.cantidad)
        param.put("pre", artM.art_m!!.pvp)
        param.put("por_dto", 0)
        param.put("reg_iva_vta",  preferences.getEmpM().reg_iva_vta)
        param.put("und_med", artM.art_m!!.und_med_vta)

        Log.e("TAG", "endpoint $endpoint")

        Log.e("TAG", "json de linea de factura "+param.toString())


        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                //message.postValue(context.getString(R.string.error_api)+" ${e.message}")
                pedido.postValue(Resource.error(context.getString(R.string.error_api) + " ${e.message}", null))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "response en linea de factura $response")

                if(response.contains("El API Key de la solicitud no es válido")){
//                    message.postValue("El API Key de la solicitud no es válido")
                    pedido.postValue(Resource.error("El API Key de la solicitud no es válido", null))

                }else
                    if (response.isEmpty() || response == "{}") {
                        //message.postValue(context.getString(R.string.error_api))
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))

                    } else {
                        try {
                            var data : ResponseVtaPedLinG = Gson().fromJson(response, ResponseVtaPedLinG::class.java)
                            if(data.count>=1) {
                                synchronized(this) {
                                    contador++
                                    Log.e("TAG", "contador $contador")
                                    Log.e("TAG", "size "+pedido_articulo_list.size)

                                    if(contador==pedido_articulo_list.size){
                                        Log.e("TAG", "contador igual a pedido ")
                                        pedido_selected.is_sincronizado=true
                                        pedido_selected.status= StatusPedido.SINCRONIZADO.name
                                        daoPedidos.update(pedido_selected)

                                        pedido.postValue(Resource.error("UPDATE", pedido_selected))
                                        pedido.postValue(Resource.error("Pedido sincronizado exitosamente", pedido_selected))
                                        nuevoPedido()
                                        updateListPedido()
                                    }

                                }


                            }else{
                                //pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                            }

                        } catch (e: java.lang.Exception) {
                            Log.e("TAG", "error al enviar la linea ${e.message}")
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))

                        }



                    }


            }
        })



    }

    private fun enviarCabecera() {


        var endpoint= preferences.getBasrUrl()+"v1/vta_ped_g?api_key="+ (preferences.getLoginInfo()?.api_key ?: "")

        var date= SimpleDateFormat("yyyy-MM-dd")
        var hora= SimpleDateFormat("HH:mm:ss")

        var fecha= SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(pedido_selected.hora)
        Log.e("TAG", "FECHA A ENVIAR $fecha")
        var param = JSONObject()
        param.put("id", 0)
        param.put("emp_div", preferences.getEmpM().id)
        //param.put("fch", date.format(fecha)+"T"+hora.format(fecha)+".000Z")
        param.put("fch", date.format(fecha))
        param.put("ser", preferences.getEmpM().ser_vta_ped)
        param.put("clt", pedido_selected.cliente!!.id)
        param.put("cmr", preferences.getUsrM().ent_rel) /***/
        param.put("alm", preferences.getUsrM().alm) /***/
        param.put("obs", pedido_selected.obs)
        param.put("nro_ord_com_clt", "") //vacio por ahora
        param.put("pre_con_iva_inc", true) //true siempre
        param.put("lat", pedido_selected.lat) //latitud
        param.put("lon", pedido_selected.lon) //longitud
        param.put("alt", pedido_selected.alt) //altura
        param.put("pre", pedido_selected.pre) //precision
        param.put("vel", pedido_selected.vel) // velocidad
        param.put("api_key", preferences.getLoginInfo()!!.api_key)

        if(daoTarifas.queryForAll().isEmpty()){
            param.put("vta_tar_g", 1)
        }else
            param.put("vta_tar_g", pedido_selected.vta_tar)


        if(pedido_selected.direccion!=null){
            param.put("cnd", pedido_selected.direccion!!.dir_etq)
        }else
            param.put("cnd", "")


        Log.e("TAG", "endpoint $endpoint")

        Log.e("TAG", "json en cabecera de factura "+param.toString())


        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                //message.postValue(context.getString(R.string.error_api)+" ${e.message}")
                pedido.postValue(Resource.error(context.getString(R.string.error_api) + " ${e.message}", null))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "response en cabecera de factura $response")

                if(response.contains("El API Key de la solicitud no es válido")){
//                    message.postValue("El API Key de la solicitud no es válido")
                    pedido.postValue(Resource.error("El API Key de la solicitud no es válido", null))

                }else
                    if (response.isEmpty() || response == "{}") {
                        //message.postValue(context.getString(R.string.error_api))
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))

                    } else {
                        try {
                            var data : ResponseVtaPedG = Gson().fromJson(response, ResponseVtaPedG::class.java)
                            if(data.count>0) {
                                pedido_selected.id_sincronizado= data.vta_ped_g[0].id
                                daoPedidos.update(pedido_selected)
                                var cont=1

                                Log.e("TAG", "LISTA DE ARTICULOS $pedido_articulo_list")

                                contador=0
                                pedido_articulo_list.forEach {
                                    //it.art_m= daoArticulos.queryForId(it.art_m!!.id!!)
                                    //it.art_m= calculatePrecio(daoArticulos.queryForId(it.art_m!!.id!!))
                                    enviarLinea(it, cont, data.vta_ped_g[0])
                                    cont++
                                }


                            }else{
                                //pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                            }


                        } catch (e: java.lang.Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))

                        }



                    }


            }
        })




    }

    private fun consumeEnt_m() {
        Log.e("TAG", "consumeEnt_m")

        var endpoint= preferences.getBasrUrl()+"v1/ent_m?filter[id_es_clt]=&api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                //message.postValue(context.getString(R.string.error_api)+" ${e.message}")
                pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO ent_m $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
//                    message.postValue("El API Key de la solicitud no es válido")
                    pedidos.postValue(Resource.error("El API Key de la solicitud no es válido", arrayListOf()))

                }else
                    if (response.isEmpty() || response == "{}") {
                        //message.postValue(context.getString(R.string.error_api))
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))

                    } else {

                        try {
                            var data : ResponseEntM = Gson().fromJson(response, ResponseEntM::class.java)
                            if(data.count>=1) {
                                daoClientes.removeAll()
                                data.ent_m.forEach { x ->
                                    daoClientes.add(x)
                                }
                            }

                            consumeDir_m()
                            /*CONT+=1
                            if(CONT>=4){
                                updateListPedido()
                            }*/

                        } catch (e: java.lang.Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))

                        }

                    }


            }
        })
    }

    private fun consumeArt_m(pag: Int, nextPage: Boolean) {

        var endpoint=preferences.getBasrUrl()+"v1/art_m?api_key=${preferences.getLoginInfo()!!.api_key}&page[number]=$pag"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "consumeArt_m $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                pedidos.postValue( Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO art_m $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    pedidos.postValue(Resource.error("El API Key de la solicitud no es válido", arrayListOf()))
                }else
                    if (response.isEmpty() || response == "{}") {
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                    } else {

                        try {
                            var data : ResponseArtM = Gson().fromJson(response, ResponseArtM::class.java)
                            if(data.count>=1) {
                                if(pag==1) {
                                    daoArticulos.removeAll()
                                }

                                data.art_m.forEach { x ->
                                    daoArticulos.add(x)
                                }

                                if(nextPage){
                                    var div= data.total_count/data.count
                                    Log.e("TAG", "RESULTADO DE LA DIVISION $div  ${data.count!= 1000}")
                                    if(pag<=div){
                                        val p= pag+1
                                        consumeArt_m(p, data.count!= 1000)
                                    }
                                }else
                                    consumeTarifas()
                            }
                            //consumeTarifas()
                            /*CONT+=1
                            if(CONT>=4){
                                updateListPedido()
                                //pedidos.postValue( Resource.success(daoPedidos.queryForAll()))
                            }


                            Log.e("TAG", "RSULTADO art_m $CONT")*/

                        } catch (e: java.lang.Exception) {
                            Log.e("TAG", "ERROR EN consumeArt_m ${e.message}")

                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        }

                    }


            }
        })

    }

    private fun consumeDir_m() {

        Log.e("TAG", "consumeDir_m")

        var endpoint= preferences.getBasrUrl()+"v1/dir_m?api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL  $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO dir_m $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    pedidos.postValue(Resource.error("El API Key de la solicitud no es válido", arrayListOf()))
                }else
                    if (response.isEmpty() || response == "{}") {
                        pedidos.postValue( Resource.error(context.getString(R.string.error_api), arrayListOf()))
                    } else {

                        try {
                            var data : ResponseDirM = Gson().fromJson(response, ResponseDirM::class.java)

                            if(data.count>=1) {
                              daoDirectorio.removeAll()

                                data.dir_m.forEach {x ->
                                    daoDirectorio.add(x)
                                }

                            }

                            consumeArt_m(1, true)
                            /*CONT+=1
                            if(CONT>=4){
                                updateListPedido()
                                //pedidos.postValue(Resource.success(daoPedidos.queryForAll()))
                            }*/


                        } catch (e: java.lang.Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        }

                    }


            }
        })
    }

    private fun consumeTarifas() {
        Log.e("TAG", "consumeTarifas")

        var endpoint= preferences.getBasrUrl()+"v1/vta_tar_g?api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL  $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO vta_tar_g $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    pedidos.postValue(Resource.error("El API Key de la solicitud no es válido", arrayListOf()))
                }else
                    if (response.isEmpty() || response == "{}") {
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                    } else {

                        try {
                            var data : ResponseVtaTarG = Gson().fromJson(response, ResponseVtaTarG::class.java)

                            if(data.count>=1) {
                                daoTarifas.removeAll()

                                data.vta_tar_g.forEach {x ->
                                    daoTarifas.add(x)
                                }

                            }
                            consumePrecios()
                            /*CONT+=1
                            if(CONT>=4){
                                updateListPedido()
                                //pedidos.postValue(Resource.success(daoPedidos.queryForAll()))
                            }*/
                        } catch (e: java.lang.Exception) {
                            Log.e("TAG", "ERROR EN consumeTarifas ${e.message}")
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        }

                    }
            }
        })
    }

    private fun consumePrecios() {
        Log.e("TAG", "consumePrecios")
        var endpoint= preferences.getBasrUrl()+"v1/vta_tar_art_g?api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL  $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    pedidos.postValue(Resource.error("El API Key de la solicitud no es válido", arrayListOf()))
                }else
                    if (response.isEmpty() || response == "{}") {
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                    } else {

                        try {
                            var data : ResponseVtaTarArtG = Gson().fromJson(response, ResponseVtaTarArtG::class.java)

                            if(data.count>=1) {
                                daoPrecios.removeAll()
                                data.vta_tar_art_g.forEach {x ->
                                    daoPrecios.add(x)
                                }
                            }

                            consumeTarifaCliente()
                            Log.e("TAG", "RSULTADO consumePrecios $response")

                        } catch (e: java.lang.Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        }

                    }
            }
        })
    }

    private fun consumeTarifaCliente() {
        Log.e("TAG", "consumeTarifaCliente")

        var endpoint= preferences.getBasrUrl()+"v1/vta_tar_cli_g?api_key=${preferences.getLoginInfo()!!.api_key}"
        Log.e("TAG", "endpoint $endpoint")

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL  $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    pedidos.postValue(Resource.error("El API Key de la solicitud no es válido", arrayListOf()))
                }else
                    if (response.isEmpty() || response == "{}") {
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                    } else {

                        try {
                            var data : ResponseVtaTarCliG = Gson().fromJson(response, ResponseVtaTarCliG::class.java)

                            if(data.count>=1) {
                                daoTarifaCliente.removeAll()

                                data.vta_tar_cli_g.forEach {x ->
                                    daoTarifaCliente.add(x)
                                }
                            }
                            consumeMon_c()
                            Log.e("TAG", "RSULTADO consumeTarifaCliente $response")

                        } catch (e: java.lang.Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))
                        }

                    }
            }
        })
    }
    private fun consumeMon_c() {
        Log.e("TAG", "consumeMon_c")
        //ttp://tienda.vlatamweb.com/API/vLatamERP_db_dat/v1/mon_c?api_key=tienda123
        var endpoint= preferences.getBasrUrl()+"v1/mon_c?api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                //message.postValue(context.getString(R.string.error_api)+" ${e.message}")
                pedidos.postValue(Resource.error(context.getString(R.string.error_api)+" ${e.message}", arrayListOf()))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO mon_c $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
//                    message.postValue("El API Key de la solicitud no es válido")
                    pedidos.postValue(Resource.error("El API Key de la solicitud no es válido", arrayListOf()))

                }else
                    if (response.isEmpty() || response == "{}") {
                        //message.postValue(context.getString(R.string.error_api))
                        pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))

                    } else {

                        try {
                            var data = Gson().fromJson(response, ResponseMonC::class.java)
                            if(data.count>=1) {
                                daoMoneda.removeAll()
                                data.mon_c.forEach { x ->
                                    daoMoneda.add(x)
                                }
                            }
                            updateListPedido()
                            pedido.postValue(Resource.error("UPDATE VISTA", null))//PARA OCULTAR LA BARRA AMARILLA
                        } catch (e: java.lang.Exception) {
                            pedidos.postValue(Resource.error(context.getString(R.string.error_api), arrayListOf()))

                        }

                    }


            }
        })
    }

    fun sharePedido() {
        var mHandler = Handler()

        Thread(Runnable {
            mHandler.post {
                generateFactura(false)
            }
        }).start()

    }

    fun imprimir() {

        var mHandler = Handler()

        Thread(Runnable {
            mHandler.post {
                generateFactura(true)
            }
        }).start()
    }

    private lateinit var cliente_qr: SearchResponse


    fun nuevoPedidoQr(cliente: SearchResponse) {
        cliente_qr= cliente
        Log.e("TAG", "CLIENTE SELECCIONADO $cliente")
        var cliente= cliente.obj as ClienteDireccion

        daoPedidos.queryForAll().forEach {
            if(it.cliente!!.id == cliente.cliente.id){
                pedido.postValue(Resource.error("Cliente ya tiene un pedido asignado", null))
                return
            }
        }

        var pedidos = pedido_qr.split("#")

        if(location!=null){
            Log.e("TAG", "getLastLocation "+location!!.latitude+"  "+ location!!.longitude)


            var date = SimpleDateFormat("dd/MM/yy HH:mm:ss").format(Date())
            var pedido = Pedido()
            pedido.is_editable = false
            pedido.cliente = cliente.cliente

            if (cliente.direccion != null)
                pedido.direccion = cliente.direccion


            pedido.lat = location!!.latitude
            pedido.lon = location!!.longitude
            pedido.alt = location!!.altitude
            pedido.pre = location!!.accuracy
            pedido.vel = location!!.speed

            pedido.hora=  SimpleDateFormat("dd/MM/yyyy hh:MM:ss").format(Date())

            Log.e("TAG", "PEDIDO A GUARDAR $pedido")
            daoPedidos.add(pedido)

            var aux = daoPedidos.getUltimoPedido()
            Log.e("TAG", "PEDIDO GUARDADO $aux")

            var list_articulos= arrayListOf<PedidoArticulo>()
            var total = 0f
            var total_art = 0f

            pedidos.forEach {
                if(it.isNotEmpty()){
                    var articulos = it.split(";")
                    val art = daoArticulos.queryForId(articulos[0].toInt())

                    if (art != null) {
                        var cant= articulos[1].toFloat()
                        var p_a= PedidoArticulo()
                        p_a.art_m= art
                        p_a.pedido=  aux
                        p_a.cantidad= cant
                        p_a.isEditable= false

                        daoPedidoArticulo.add(p_a)
                        list_articulos.add(p_a)

                        total+= art.pvp!! *cant
                        total_art+=cant

                    }
                }

            }

            aux.total= total
            aux.total_articulos= total_art
            daoPedidos.update(aux)

            /*for (pedidoArticulo in pedido_articulo_list) {
                pedidoArticulo.pedido = aux
                Log.e("TAG", "artiuclos $pedidoArticulo")
                daoPedidoArticulo.add(pedidoArticulo)
            }*/

            updateListPedido()

            nuevoPedido()


        } else {
            Log.e("TAG", "getLastLocation:exception ")

            pedido.postValue(Resource.error("3", null))
            create_pedido_qr=true
            prepareLocation()

        }



    }

    private lateinit var pedido_qr: String

    fun nuevoPedidoQr(result: String) {
        pedido_qr=result
        var data : MutableList<SearchResponse> = arrayListOf()

        daoClientes.queryForAll().forEach {it ->
            var direcciones= daoDirectorio.queryForAll(it.id!!)

            if(direcciones.size>0){
                for (dir in direcciones) {
                    data.add(SearchResponse(it.nom_com, ClienteDireccion(it, dir)))
                }
            }else
                data.add(SearchResponse(it.nom_com, ClienteDireccion(it, null)))
        }

        Log.e("TAG", "lista de lientes en qr "+data.size)
        if(!data.isEmpty())
            data= data.sortedBy{ it.name} as MutableList<SearchResponse>


        searchList.value= Resource.loading(data)

    }


    @SuppressLint("NewApi")
    private fun generateFactura(imprimir: Boolean) {

        var fecha= SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(pedido_selected.hora)
        var date= SimpleDateFormat("dd-MM-yy")
        var date_form= SimpleDateFormat("ddMMyyyyHHmm")
        val formato = DecimalFormat("#,###.00")


        createFile("PED-${date_form.format(fecha)}")
        createDocument()
        document.open()


        try {

            var table= PdfPTable(2)
            table.widthPercentage=100f

            var table_empresa= PdfPTable(1)

            if(preferences.getEntM().img.isNotEmpty()){
                val decodedString = Base64.decode(preferences.getEntM().img, Base64.DEFAULT)

                val bmp= BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                val stream = ByteArrayOutputStream()
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val image = Image.getInstance(stream.toByteArray())

                image.scalePercent(50f)

                var cellImage= PdfPCell(image)
                cellImage.border= Rectangle.NO_BORDER
                table_empresa.addCell(cellImage)

            }else{
                val d = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    context.getDrawable(R.drawable.logo_vlatam_tienda)
                } else {
                    return
                }

                val bitDw = d as BitmapDrawable
                val bmp = bitDw.bitmap
                val stream = ByteArrayOutputStream()
                bmp.compress(Bitmap.CompressFormat.JPEG, 50, stream)
                var byteArray = stream.toByteArray()

                val image = Image.getInstance(byteArray)

                var cellImage= PdfPCell(image)
                cellImage.border= Rectangle.NO_BORDER
                table_empresa.addCell(cellImage)
            }


            var cell22= PdfPCell(Paragraph(preferences.getEntM().nom_com))
            cell22.border= Rectangle.NO_BORDER
            table_empresa.addCell(cell22)

            cell22= PdfPCell(Paragraph(preferences.getEntM().cif))
            cell22.border= Rectangle.NO_BORDER
            table_empresa.addCell(cell22)

            cell22= PdfPCell(Paragraph(preferences.getEntM().dir))
            cell22.border= Rectangle.NO_BORDER
            table_empresa.addCell(cell22)

            if(!preferences.getEntM().tlf.isEmpty()){
                cell22= PdfPCell(Paragraph(preferences.getEntM().tlf))
                cell22.border= Rectangle.NO_BORDER
                table_empresa.addCell(cell22)
            }

            //table.addCell(table_empresa)

            var cell_emp= PdfPCell(table_empresa)
            cell_emp.borderColor= BaseColor(97, 97, 97)
            cell_emp.border= Rectangle.NO_BORDER
            table.addCell(cell_emp)


            //header del pedido
            var table_header= PdfPTable(1)

            var cellHeader= PdfPCell(Paragraph("Pedido de Venta   PED-${date_form.format(fecha)}"))
            cellHeader.border= Rectangle.NO_BORDER
            table_header.addCell(cellHeader)


            var ce11= PdfPCell(Paragraph("________________________________"))
            ce11.borderColor= BaseColor(97, 97, 97)
            ce11.border= Rectangle.NO_BORDER
            table_header.addCell(ce11)

            cellHeader= PdfPCell(Paragraph("Fecha de Pedido     ${date.format(fecha)}"))
            cellHeader.border= Rectangle.NO_BORDER
            cellHeader.paddingBottom= 10f
            table_header.addCell(cellHeader)


            cellHeader= PdfPCell(Paragraph("Elaborado por: ${ preferences.getEntM().nom_com}"))
            cellHeader.border= Rectangle.NO_BORDER
            table_header.addCell(cellHeader)


            cellHeader= PdfPCell(Paragraph("   "))
            cellHeader.border= Rectangle.NO_BORDER
            table_header.addCell(cellHeader)
            table_header.addCell(cellHeader)

            cellHeader= PdfPCell(Paragraph("Cliente "))
            cellHeader.border= Rectangle.NO_BORDER
            table_header.addCell(cellHeader)

            ce11= PdfPCell(Paragraph("________________________________"))
            ce11.borderColor= BaseColor(97, 97, 97)
            ce11.border= Rectangle.NO_BORDER
            table_header.addCell(ce11)


            cellHeader= PdfPCell(Paragraph(pedido_selected.cliente!!.nom_com))
            cellHeader.border= Rectangle.NO_BORDER
            table_header.addCell(cellHeader)

            cellHeader= PdfPCell(Paragraph("Doc:  ${pedido_selected.cliente!!.cif}"))
            cellHeader.border= Rectangle.NO_BORDER
            table_header.addCell(cellHeader)


            if(pedido_selected.direccion!=null){
                cellHeader= PdfPCell(Paragraph("${pedido_selected.direccion!!.dir}"))
                cellHeader.border= Rectangle.NO_BORDER
                table_header.addCell(cellHeader)
            }

            var cell= PdfPCell(table_header)
            cell.borderColor= BaseColor(97, 97, 97)
            cell.border= Rectangle.NO_BORDER
            table.addCell(cell)


            document.add(table)


        } catch (ex: IOException) {
            return
        }

        document.add(Paragraph("\n"))


        var table_art= PdfPTable(floatArrayOf(1.5f, 4f, 0.5f, 1f, 0.5f, 1f))
        table_art.widthPercentage=100f

        var cell_p= PdfPCell(Paragraph("Artículos"))
        cell_p.colspan=6
        cell_p.borderColor= BaseColor(97, 97, 97)
        cell_p.border= Rectangle.BOTTOM
        cell_p.paddingBottom= 10f
        table_art.addCell(cell_p)


        var cell1= PdfPCell(Paragraph("Refª."))
        cell1.borderColor= BaseColor(97, 97, 97)
        cell1.border= Rectangle.BOTTOM
        cell_p.paddingBottom= 10f
        table_art.addCell(cell1)

        cell1= PdfPCell(Paragraph("Descripción"))
        cell1.borderColor= BaseColor(97, 97, 97)
        cell1.border= Rectangle.BOTTOM
        cell_p.paddingBottom= 10f
        table_art.addCell(cell1)

        cell1= PdfPCell(Paragraph("Cant."))
        cell1.borderColor= BaseColor(97, 97, 97)
        cell1.border= Rectangle.BOTTOM
        cell_p.paddingBottom= 10f
        table_art.addCell(cell1)

        cell1= PdfPCell(Paragraph("Precio ud."))
        cell1.borderColor= BaseColor.DARK_GRAY
        cell1.border= Rectangle.BOTTOM
        cell_p.paddingBottom= 10f
        table_art.addCell(cell1)

        cell1= PdfPCell(Paragraph("Dto."))
        cell1.borderColor= BaseColor(97, 97, 97)
        cell1.border= Rectangle.BOTTOM
        cell_p.paddingBottom= 10f
        table_art.addCell(cell1)

        cell1= PdfPCell(Paragraph("Importe"))
        cell1.borderColor= BaseColor(97, 97, 97)
        cell1.border= Rectangle.BOTTOM
        cell_p.paddingBottom= 10f
        table_art.addCell(cell1)


        var base=0f
        var impuesto=0f

        pedido_articulo_list.forEach {

            cell1= PdfPCell(Paragraph(it.art_m!!.ref))
            cell1.borderColor= BaseColor(97, 97, 97)
            cell1.border= Rectangle.BOTTOM
            cell_p.paddingBottom= 10f
            table_art.addCell(cell1)


            cell1= PdfPCell(Paragraph(it.art_m!!.name))
            cell1.borderColor= BaseColor(97, 97, 97)
            cell1.border= Rectangle.BOTTOM
            cell_p.paddingBottom= 10f
            table_art.addCell(cell1)

            cell1= PdfPCell(Paragraph(it.cantidad.toString()))
            cell1.borderColor= BaseColor(97, 97, 97)
            cell1.border= Rectangle.BOTTOM
            cell_p.paddingBottom= 10f
            cell1.horizontalAlignment= PdfPCell.ALIGN_CENTER
            table_art.addCell(cell1)

            cell1= PdfPCell(Paragraph(formato.format(it.art_m!!.pvp)))
            cell1.borderColor= BaseColor(97, 97, 97)
            cell1.border= Rectangle.BOTTOM
            cell_p.paddingBottom= 10f
            cell1.horizontalAlignment= PdfPCell.ALIGN_RIGHT
            table_art.addCell(cell1)

            cell1= PdfPCell(Paragraph("   "))
            cell1.borderColor= BaseColor(97, 97, 97)
            cell1.border= Rectangle.BOTTOM
            cell_p.paddingBottom= 10f
            table_art.addCell(cell1)

            var total= it.art_m!!.pvp!! * it.cantidad
            var b=0f
            var iva=0f

            when(it.art_m!!.reg_iva_vta){
                "G"->{
                    b= total/ (1 + (preferences.getEmpM().por_iva_gen / 100) )
                    iva = b * (preferences.getEmpM().por_iva_gen / 100)
                }

                "R"->{
                    b= total/ (1 + (preferences.getEmpM().por_iva_red / 100) )
                    iva = b * (preferences.getEmpM().por_iva_gen / 100)
                }
                "S"->{
                    b= total/ (1 + (preferences.getEmpM().por_iva_sup / 100) )
                    iva = b * (preferences.getEmpM().por_iva_gen / 100)
                }
                "E"->{
                    b= total/ (1 + (preferences.getEmpM().por_iva_esp / 100) )
                    iva = b * (preferences.getEmpM().por_iva_gen / 100)
                }
                "X"->{
                    b= total
                    iva = 0f
                }
            }

            base+=b
            impuesto+=iva

            cell1= PdfPCell(Paragraph(formato.format(total)+"        "))
            cell1.borderColor= BaseColor(97, 97, 97)
            cell1.border= Rectangle.BOTTOM
            cell1.horizontalAlignment= PdfPCell.ALIGN_RIGHT
            cell_p.paddingRight= 20f
            table_art.addCell(cell1)

        }

        document.add(table_art)

        document.add(Paragraph("\n"))
        document.add(Paragraph("\n"))
        document.add(Paragraph("\n"))
        document.add(Paragraph("\n"))


        var table= PdfPTable(2)
        table.widthPercentage=100f

        var cell= PdfPCell(Paragraph("Condiciones"))
        cell.borderColor= BaseColor(97, 97, 97)
        cell.border= Rectangle.NO_BORDER
        table.addCell(cell)

        cell= PdfPCell(Paragraph("Resumen"))
        cell.borderColor= BaseColor(97, 97, 97)
        cell.border= Rectangle.NO_BORDER
        table.addCell(cell)


        cell= PdfPCell(Paragraph("________________________________"))
        cell.borderColor= BaseColor(97, 97, 97)
        cell.border= Rectangle.NO_BORDER
        table.addCell(cell)
        cell= PdfPCell(Paragraph("______________________________________"))
        cell.borderColor= BaseColor(97, 97, 97)
        cell.border= Rectangle.NO_BORDER
        cell.horizontalAlignment= PdfPCell.ALIGN_RIGHT
        table.addCell(cell)


        var cell_vacia= PdfPCell(Paragraph("  "))
        cell_vacia.borderColor= BaseColor(97, 97, 97)
        cell_vacia.border= Rectangle.NO_BORDER
        table.addCell(cell_vacia)




        cell= PdfPCell(Paragraph("Base Imponible ${formato.format(base)}"))
        cell.borderColor= BaseColor(97, 97, 97)
        cell.border= Rectangle.NO_BORDER
        cell.horizontalAlignment= PdfPCell.ALIGN_RIGHT
        table.addCell(cell)


        table.addCell(cell_vacia)

        cell= PdfPCell(Paragraph("Impuesto ${formato.format(impuesto)}"))
        cell.borderColor= BaseColor(97, 97, 97)
        cell.border= Rectangle.NO_BORDER
        cell.horizontalAlignment= PdfPCell.ALIGN_RIGHT
        cell.paddingBottom= 15f
        table.addCell(cell)


        table.addCell(cell_vacia)


        cell= PdfPCell(Paragraph("_____________________"))
        cell.borderColor= BaseColor(97, 97, 97)
        cell.border= Rectangle.NO_BORDER
        cell.horizontalAlignment= PdfPCell.ALIGN_RIGHT
        table.addCell(cell)
        table.addCell(cell_vacia)


        cell= PdfPCell(Paragraph("Total ${formato.format(base+impuesto)}"))
        cell.borderColor= BaseColor(97, 97, 97)
        cell.border= Rectangle.NO_BORDER
        cell.horizontalAlignment= PdfPCell.ALIGN_RIGHT
        cell.paddingBottom= 15f
        table.addCell(cell)

        table.horizontalAlignment= Element.ALIGN_CENTER
        document!!.add(table)

        document.close()
        Log.e("TAG", "RUTAAA ${file.absolutePath}")

        if(imprimir){
            file_d.postValue(Resource.loading(file))
        }else
            file_d.postValue(Resource.success(file))

    }


    fun createFile(name: String){
        //var fpath = "${}/$name.pdf"
        file = File(Environment.getExternalStorageDirectory(), "$name.pdf")
        if (!file.exists()) {
            file.createNewFile()
        }
    }

    fun createDocument(){
        document = Document(PageSize.A4)
        var writer= PdfWriter.getInstance(document, FileOutputStream(file.absoluteFile))
        //al event = HeaderFooterPageEvent(pedido_selected.total.toFloat(), 12f)
        //writer.pageEvent = event
    }

    fun enviarToken(){
        try{
            val refreshedToken = FirebaseInstanceId.getInstance().token

            if(refreshedToken!= null){

                var endpoint= preferences.getBasrUrl()+"/ACT_TOK"

                var param = JSONObject()

                param.put("token", refreshedToken)
                param.put("proyecto", "vLatamTienda" )

                var JSON= MediaType.parse("application/json; charset=utf-8")

                var body = RequestBody.create(JSON, param.toString())

                var request = Request.Builder()
                        .url(endpoint)
                        .post(body)
                        .build()



                client.newCall(request).enqueue(object: Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        Log.e("TAG", "error al enviar token "+e.message)
                    }

                    override fun onResponse(call: Call, response_mesa_t: Response) {
                        var response: String =response_mesa_t.body()!!.string()
                        Log.e("TAG", "token enviado $response")

                    }
                })
            }
        }catch (ex: Exception){
            Log.e("TAG", "error al enviar token ")
        }


    }


}

val moduleMain= module{
    viewModel{ MainViewModel(get()) }
    //single { PrefrencesManager(get()) }
//    single { PreferenceManager.getDefaultSharedPreferences(VlatamTiendaApplication.instance.getContext()) }
}