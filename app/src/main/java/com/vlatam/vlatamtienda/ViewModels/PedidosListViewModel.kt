package com.vlatam.vlatamtienda.ViewModels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.util.Log
import com.vlatam.vlatamtienda.Helper.Resource
import com.google.gson.Gson
import com.vlatam.vlatamtienda.Data.Local.*
import com.vlatam.vlatamtienda.Data.Repository.*
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.Servicios.PrefrencesManager
import com.vlatam.vlatamtienda.Servicios.ServicioPedidos
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import okhttp3.*
import org.json.JSONObject
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class PedidosListViewModel constructor(val preferences: PrefrencesManager): ViewModel() {
    private val client= OkHttpClient()
    private val context= VlatamTiendaApplication.instance.getContext()

    private val daoPedidos = PedidoDao()
    private val daoClientes = ClienteDao()
    private val daoArticulos= ArticulosDao()
    private val daoDirectorio= DirectorioDao()


    val pedidos_pendientes = MutableLiveData<Resource<MutableList<PedidoSlected>>> () /**  lista de pedidos del menu drawer **/
    var pendientes: MutableList<Pedido> = arrayListOf()

    val pedidos_sincronizados = MutableLiveData<Resource<MutableList<PedidoGroup>>> () /**  lista de pedidos del menu drawer **/
    var sincronizados: MutableList<Pedido> = arrayListOf()
    var sincronizados_group: MutableList<PedidoGroup> = arrayListOf()

    val pedidos_selected = MutableLiveData<Resource<MutableList<PedidoSlected>>> () /**  lista de pedidos del menu drawer **/
    var selected: MutableList<Pedido> = arrayListOf()


    var pedido_selected= Pedido()
    var pedido= MutableLiveData<Resource<Pedido>>()


   // var ent_m: ClienteDireccion? = null  /** cliente de un pedido_selected **/
    var pedido_articulo_list: MutableList<PedidoArticulo> = arrayListOf()

    //var artMList= MutableLiveData<Resource<MutableList<PedidoArticulo>>> () /**  lista de articulos de un pedido_selected **/
    //val cliente = MutableLiveData<Resource<ClienteDireccion>> ()  /** cliente de un pedido_selected **/

    private var contador=0




    fun toPedidoSelected(list: MutableList<Pedido>): MutableList<PedidoSlected>{
        var pedidos: MutableList<PedidoSlected> = arrayListOf()

        list.forEach {
            pedidos.add(PedidoSlected(it, false))
        }

        return  pedidos
    }
    fun updateListPedido() {

        pendientes= daoPedidos.queryForAllPendientes()

        if(!pendientes.isEmpty()){
            pendientes= pendientes.sortedByDescending { it.id } as MutableList<Pedido>

            pendientes.forEach{ x ->
                x.cliente= daoClientes.queryForId(x.cliente!!.id!!)
                if(x.direccion!=null)
                    x.direccion= daoDirectorio.queryForId(x.direccion!!.id!!)
            }

            pedidos_pendientes.postValue(Resource.success(toPedidoSelected(pendientes)))
        }else{
            pedidos_pendientes.postValue(Resource.success(arrayListOf()))
        }

    }

    fun updateListSincronizados() {

        sincronizados= daoPedidos.queryForAllSincronizados()

        if(!sincronizados.isEmpty()) {
            sincronizados = sincronizados.sortedByDescending { it.id } as MutableList<Pedido>

            sincronizados.forEach { x ->
                x.cliente = daoClientes.queryForId(x.cliente!!.id!!)
                if (x.direccion != null)
                    x.direccion = daoDirectorio.queryForId(x.direccion!!.id!!)
            }

        }else{
            pedidos_sincronizados.postValue(Resource.success(arrayListOf()))
            return
        }

        val byFecha = sincronizados.groupBy { it.hora.split(" ")[0] }




        Log.e("TAG", byFecha.keys.toString()) // [1, 3, 2, 4]
        Log.e("TAG", byFecha.values.toString()) // [[a], [abc, def], [ab], [abcd]]



        byFecha.keys.sortedByDescending { it }.forEachIndexed{ index, s ->
            var ped= PedidoGroup(toPedidoSelected(byFecha.values.toMutableList()[index].toMutableList()), s, false)
            sincronizados_group.add(ped)
        }

        pedidos_sincronizados.postValue(Resource.success(sincronizados_group))

    }

    fun sincronizar(){
        val intent = Intent(context, ServicioPedidos::class.java)
        val lista= ListPedidos(selected)
        Log.e("TAG", "DATA ${Gson().toJson(lista)}")
        intent.putExtra("data", Gson().toJson(lista))
        context.startService(intent)
    }

    fun sincronizar(p: Pedido){
        pedido_selected.obs= p.obs

        Log.e("TAG", "PEDIDO ACTUALIZADO "+pedido_selected)
        enviarCabecera()
    }

    private fun enviarCabecera() {


        var endpoint= preferences.getBasrUrl()+"v1/vta_ped_g?api_key="+ (preferences.getLoginInfo()?.api_key ?: "")

        var date= SimpleDateFormat("yyyy-MM-dd")
        var hora= SimpleDateFormat("HH:mm:ss")

        var param = JSONObject()
        param.put("id", 0)
        param.put("emp_div", preferences.getEmpM().id)
        param.put("fch", date.format(Date(pedido_selected.hora))+"T"+hora.format(Date(pedido_selected.hora))+".000Z")
        param.put("ser", preferences.getEmpM().ser_vta_ped)
        param.put("clt", pedido_selected.cliente!!.id)
        param.put("cmr", preferences.getUsrM().ent_rel) /***/
        param.put("alm", preferences.getUsrM().alm) /***/
        param.put("obs", pedido_selected.obs)
        param.put("nro_ord_com_clt", "") //vacio por ahora
        param.put("pre_con_iva_inc", true) //true siempre
        param.put("lat", pedido_selected.lat) //latitud
        param.put("lon", pedido_selected.lon) //longitud
        param.put("alt", pedido_selected.alt) //altura
        param.put("pre", pedido_selected.pre) //precision
        param.put("vel", pedido_selected.vel) // velocidad
        param.put("api_key", preferences.getLoginInfo()!!.api_key)

        if(pedido_selected.direccion!=null){
            param.put("cnd", pedido_selected.direccion!!.dir_etq)
        }else
            param.put("cnd", "")


        Log.e("TAG", "endpoint $endpoint")

        Log.e("TAG", "json en cabecera de factura "+param.toString())


        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                //message.postValue(context.getString(R.string.error_api)+" ${e.message}")
                pedido.postValue(Resource.error(context.getString(R.string.error_api) + " ${e.message}", null))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "response en cabecera de factura $response")

                if(response.contains("El API Key de la solicitud no es válido")){
                    pedido.postValue(Resource.error("El API Key de la solicitud no es válido", null))

                }else
                    if (response.isEmpty() || response == "{}") {
                        pedido.postValue(Resource.error(context.getString(R.string.error_api), null))

                    } else {
                        try {
                            var data : ResponseVtaPedG = Gson().fromJson(response, ResponseVtaPedG::class.java)
                            if(data.count>0) {
                                pedido_selected.id_sincronizado= data.vta_ped_g[0].id
                                daoPedidos.update(pedido_selected)
                                var cont=1

                                Log.e("TAG", "LISTA DE ARTICULOS $pedido_articulo_list")

                                contador=0
                                pedido_articulo_list.forEach {
                                    it.art_m= daoArticulos.queryForId(it.art_m!!.id!!)

                                    enviarLinea(it, cont, data.vta_ped_g[0])
                                    cont++
                                }


                            }else{
                                pedido.postValue(Resource.error(context.getString(R.string.error_api), null))
                            }


                        } catch (e: java.lang.Exception) {
                            pedido.postValue(Resource.error(context.getString(R.string.error_api), null))

                        }



                    }


            }
        })




    }

    private fun enviarLinea(artM: PedidoArticulo, pos: Int, cabecera: VtaPedG){

        var endpoint= preferences.getBasrUrl()+"v1/vta_ped_lin_g?api_key="+ (preferences.getLoginInfo()?.api_key ?: "")

        var date= SimpleDateFormat("yyyy-MM-dd")
        var hora= SimpleDateFormat("HH:mm:ss")

        var param = JSONObject()
        param.put("id", 0)
        param.put("vta_ped", cabecera.id)
        param.put("num_lin", pos)
        param.put("fch", date.format(Date(pedido_selected.hora)))
        param.put("hor", date.format(Date(pedido_selected.hora))+"T"+hora.format(Date(pedido_selected.hora))+".000Z") // si elegimos un cliente sino va 0
        param.put("emp", cabecera.emp_div) /***/
        param.put("clt", cabecera.clt) /***/
        param.put("ser", cabecera.ser)
        param.put("alm", cabecera.alm)
        param.put("art", artM.art_m!!.id)
        param.put("dsc", "")
        param.put("can_ped", artM.cantidad)
        param.put("can_ped_und", artM.cantidad)
        param.put("pre", artM.art_m!!.pvp)
        param.put("por_dto", 0)
        param.put("reg_iva_vta",  preferences.getEmpM().reg_iva_vta)
        param.put("und_med", artM.art_m!!.und_med_vta)

        Log.e("TAG", "endpoint $endpoint")

        Log.e("TAG", "json de linea de factura "+param.toString())


        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                //message.postValue(context.getString(R.string.error_api)+" ${e.message}")
                pedido.postValue(Resource.error(context.getString(R.string.error_api) + " ${e.message}", null))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "response en linea de factura $response")

                if(response.contains("El API Key de la solicitud no es válido")){
                    pedido.postValue(Resource.error("El API Key de la solicitud no es válido", null))

                }else
                    if (response.isEmpty() || response == "{}") {
                        pedido.postValue(Resource.error(context.getString(R.string.error_api), null))

                    } else {
                        try {
                            var data : ResponseVtaPedLinG = Gson().fromJson(response, ResponseVtaPedLinG::class.java)
                            if(data.count>=1) {
                                contador++

                                if(contador==pedido_articulo_list.size){
                                    Log.e("TAG", "contador igual a pedido ")
                                    pedido_selected.is_sincronizado=true
                                    pedido_selected.status= StatusPedido.SINCRONIZADO.name
                                    daoPedidos.update(pedido_selected)

                                    pedido.postValue(Resource.error("UPDATE", pedido_selected))
                                    pedido.postValue(Resource.error("Pedido sincronizado exitosamente", pedido_selected))

                                    updateListPedido()
                                }


                            }else{
                                pedido.postValue(Resource.error(context.getString(R.string.error_api), null))
                            }

                        } catch (e: java.lang.Exception) {
                            pedido.postValue(Resource.error(context.getString(R.string.error_api), null))

                        }



                    }


            }
        })



    }

    fun selectedPedientes() {
        for (ped in pendientes){
            selectedPedido(PedidoSlected(ped, true))
        }
    }

    fun unselectedPedientes() {
        for (ped in pendientes){
            selectedPedido(PedidoSlected(ped, false))
        }
    }

    fun selectedPedido(it: PedidoSlected) {
        if(it.slected){
            if(!selected.contains(it.pedido))
                selected.add(it.pedido)
        }else{
            if(selected.contains(it.pedido))
                selected.remove(it.pedido)
        }
        if(selected.isEmpty()){
            pedidos_selected.value= Resource.error("", null)
        }else
            pedidos_selected.value= Resource.success(toPedidoSelected(selected))

        Log.e("TAG", "pedido seleccionados $selected")

    }




}

val modulePedidos= module{
    viewModel{ PedidosListViewModel(get()) }
}