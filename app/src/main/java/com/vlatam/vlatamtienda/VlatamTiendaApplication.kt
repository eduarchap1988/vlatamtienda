package com.vlatam.vlatamtienda

import android.content.Context
import android.support.multidex.MultiDexApplication
import com.vlatam.vlatamtienda.ViewModels.moduleLogin
import com.vlatam.vlatamtienda.ViewModels.moduleMain
import org.koin.android.ext.android.startKoin
import com.vlatam.vlatamtienda.ViewModels.modulePedidos
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import uk.co.chrisjenx.calligraphy.CalligraphyConfig





class VlatamTiendaApplication: MultiDexApplication() {
    companion object {
        private val TAG = VlatamTiendaApplication::class.java.simpleName
        val EXISTENCIA = true

        @get:Synchronized lateinit var instance: VlatamTiendaApplication
            private set
    }


    override fun onCreate() {
        super.onCreate()
        instance = this
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/roboto.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
        startKoin(this, listOf(moduleLogin, moduleMain, modulePedidos))
        Fabric.with(this, Crashlytics())
    }


    fun getContext(): Context{
        return applicationContext
    }
}