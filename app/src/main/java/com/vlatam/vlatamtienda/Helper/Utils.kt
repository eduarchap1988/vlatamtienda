package com.vlatam.vlatmrest.Helper

import android.content.Context
import android.location.LocationManager
import android.net.ConnectivityManager
import java.text.DecimalFormat

object Utils {

    fun isNetDisponible(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val actNetInfo = connectivityManager.activeNetworkInfo
        return actNetInfo != null && actNetInfo.isConnected
    }


    fun isGPSProvider(context: Context): Boolean {
        val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun obtieneDosDecimales(valor: Float): String {
        val format = DecimalFormat()
        format.maximumFractionDigits = 2 //Define 2 decimales.
        return format.format(valor)
    }
}