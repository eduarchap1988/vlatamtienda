package com.vlatam.vlatamtienda.Helper

import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.table.TableUtils
import com.vlatam.vlatamtienda.Data.Local.*
import com.vlatam.vlatamtienda.VlatamTiendaApplication


object  DBHelper : OrmLiteSqliteOpenHelper(VlatamTiendaApplication.instance, "vlatama_tienda.db", null, 3) {

    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        TableUtils.createTableIfNotExists(connectionSource, Pedido::class.java)
        TableUtils.createTableIfNotExists(connectionSource, EntM::class.java)
        TableUtils.createTableIfNotExists(connectionSource, ArtM::class.java)
        TableUtils.createTableIfNotExists(connectionSource, DirM::class.java)
        TableUtils.createTableIfNotExists(connectionSource, VtaTarG::class.java)
        TableUtils.createTableIfNotExists(connectionSource, VtaTarArtG::class.java)
        TableUtils.createTableIfNotExists(connectionSource, PedidoArticulo::class.java)
        TableUtils.createTableIfNotExists(connectionSource, VtaTarCliG::class.java)
        TableUtils.createTableIfNotExists(connectionSource, MonC::class.java)

    }

    override fun onUpgrade(database: SQLiteDatabase?, connectionSource: ConnectionSource?, oldVersion: Int, newVersion: Int) {
        TableUtils.dropTable<Pedido, Any>(connectionSource, Pedido::class.java, true)
        TableUtils.dropTable<EntM, Any>(connectionSource, EntM::class.java, true)
        TableUtils.dropTable<ArtM, Any>(connectionSource, ArtM::class.java, true)
        TableUtils.dropTable<DirM, Any>(connectionSource, DirM::class.java, true)
        TableUtils.dropTable<VtaTarG, Any>(connectionSource, VtaTarG::class.java, true)
        TableUtils.dropTable<VtaTarArtG, Any>(connectionSource, VtaTarArtG::class.java, true)
        TableUtils.dropTable<PedidoArticulo, Any>(connectionSource, PedidoArticulo::class.java, true)
        TableUtils.dropTable<VtaTarCliG, Any>(connectionSource, VtaTarCliG::class.java, true)
        TableUtils.dropTable<MonC, Any>(connectionSource, MonC::class.java, true)

        onCreate(database, connectionSource)
    }

    @Throws(SQLException::class)
    fun getPedidoDao(): Dao<Pedido, Int> {
        return getDao(Pedido::class.java)
    }

    @Throws(SQLException::class)
    fun getClienteDao(): Dao<EntM, Int> {
        return getDao(EntM::class.java)
    }


    @Throws(SQLException::class)
    fun getArticulosDao(): Dao<ArtM, Int> {
        return getDao(ArtM::class.java)
    }

    @Throws(SQLException::class)
    fun getDirectorioDao(): Dao<DirM, Int> {
        return getDao(DirM::class.java)
    }

    @Throws(SQLException::class)
    fun getTarifasDao(): Dao<VtaTarG, Int> {
        return getDao(VtaTarG::class.java)

    }

    @Throws(SQLException::class)
    fun getPreciosDao(): Dao<VtaTarArtG, Int> {
        return getDao(VtaTarArtG::class.java)
    }

    @Throws(SQLException::class)
    fun getPedidoArticuloDao(): Dao<PedidoArticulo, Int> {
        return getDao(PedidoArticulo::class.java)
    }

    @Throws(SQLException::class)
    fun getTarifaClienteDao(): Dao<VtaTarCliG, Int> {
        return getDao(VtaTarCliG::class.java)
    }

    @Throws(SQLException::class)
    fun getMonDao(): Dao<MonC, Int> {
        return getDao(MonC::class.java)
    }

}