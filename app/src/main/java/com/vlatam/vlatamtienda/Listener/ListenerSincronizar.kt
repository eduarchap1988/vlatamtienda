package com.vlatam.vlatamtienda.Listener

import com.vlatam.vlatamtienda.Data.Local.Pedido

interface ListenerSincronizar {
    fun sincronizarPedido(pedido: Pedido)
}