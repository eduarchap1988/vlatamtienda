package com.vlatam.vlatamtienda.UI.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatamtienda.Data.Local.PedidoSlected
import com.vlatam.vlatamtienda.R
import kotlinx.android.synthetic.main.item_pedido_multiple.view.*

class AdapterListPedidos (val items: MutableList<PedidoSlected>, val context: Context,
                          val listener: (Any) -> Unit): RecyclerView.Adapter<AdapterListPedidos.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        Log.e("TAG", "position $p1")
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_pedido_multiple, parent, false))

    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun haySeleccion(): Boolean {

        for (item in items) {
            if(item.slected){
                return true
            }
        }
        return false
    }


    fun selectedAll() {
        for(ped in items){
            ped.slected= true
        }
        notifyDataSetChanged()
    }

    fun unselectedAll() {
        for(ped in items){
            ped.slected= false
        }
        notifyDataSetChanged()
    }


    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: PedidoSlected, listener: (Any) -> Unit){
            view.status_value.text= item.pedido.status

            view.pedido_title.text= item.pedido.cliente!!.nom_com

            view.total_monto.text= item.pedido.total.toString()

            view.checkbox.isChecked = item.slected

            view.checkbox.setOnCheckedChangeListener { compoundButton, b ->
                item.slected=b
                listener(item)
            }

            view.setOnClickListener {
                listener(item.pedido)
            }
        }

    }
}