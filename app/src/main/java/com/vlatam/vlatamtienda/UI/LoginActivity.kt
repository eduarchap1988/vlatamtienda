package com.vlatam.vlatamtienda.UI

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.ViewModels.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.viewmodel.ext.android.viewModel
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.net.MalformedURLException
import java.net.URISyntaxException
import java.net.URL

class LoginActivity: AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModel()

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupSnackBar()
        setupOnClick()
        setupActivityIncio()
        permisos()
        loginViewModel.checkForAuthenticateUser()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))

    }


    private fun setupActivityIncio() {

        loginViewModel.getUser().observe(this, Observer<HashMap<String, String>> {it ->
            val intent = MainActivity.newIntent(this)
            intent.putExtra("nombre", it!!["nom_com"])
            intent.putExtra("img", it!!["img"])
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        })

    }

    @SuppressLint("SetTextI18n")
    private fun setupOnClick() {
        btn_iniciar.setOnClickListener{
            if(!edit_url.text.contains("http")){
                edit_url.setText("http://"+edit_url.text)
            }

            if(edit_apikey.text.toString().isEmpty() || edit_apikey.text.toString().isEmpty() )
                showMssage(getString(R.string.error_campo))
            else
                if(validate(edit_url.text.toString())) {
                    btn_iniciar.text= "INICIANDO..."
                    loginViewModel.login(edit_apikey.text.toString(), edit_apikey.text.toString(), edit_url.text.toString())
                }else{
                    showMssage(getString(R.string.error_login))
                }

        }

    }

    private fun validate(url: String): Boolean {
        /*validación de url*/
        try {
            URL(url).toURI()
            return true
        } catch (exception: URISyntaxException) {
            return false
        } catch (exception: MalformedURLException) {
            return false
        }
    }

    private fun setupSnackBar() {


        loginViewModel.message.observe(this, Observer<String> {it ->
            ocultarTeclado()
            btn_iniciar.text= "INICIAR"
            showMssage(it!!)
        })

    }


    private fun permisos() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    2)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 2) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Snackbar.make(constraintLogin, R.string.permision_available,
                        Snackbar.LENGTH_SHORT).show()
            } else {
                Snackbar.make(constraintLogin, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();

            }
        }


    }



    fun showMssage(texto: String) {
        val mSnackBar = Snackbar.make(constraintLogin, texto, Snackbar.LENGTH_LONG)
        val mainTextView = mSnackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mainTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        } else {
            mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        }
        mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        mainTextView.setTextColor(resources.getColor(R.color.colorPrimary))
        mSnackBar.show()

    }
    fun ocultarTeclado() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(constraintLogin.windowToken, 0)
    }
}