package com.vlatam.vlatamtienda.UI.Adapters

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatamtienda.Data.Local.PedidoGroup
import com.vlatam.vlatamtienda.R
import kotlinx.android.synthetic.main.item_pedido_group.view.*

class AdapterGroupListPedidos (val items: MutableList<PedidoGroup>, val context: Context,
                               val listener: (Any) -> Unit): RecyclerView.Adapter<AdapterGroupListPedidos.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], context, listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        Log.e("TAG", "position $p1")
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_pedido_group, parent, false))

    }

    override fun getItemCount(): Int {
        return items.size
    }


    fun selectedAll(grupo: PedidoGroup) {
        for(ped in items){
            if(ped == grupo) {
                if(!ped.slected){
                    ped.slected = true

                    for (pedidoSlected in ped.pedido) {
                        pedidoSlected.slected= true
                        listener.invoke(pedidoSlected)
                    }
                }else{
                    ped.slected = false

                    for (pedidoSlected in ped.pedido) {
                        pedidoSlected.slected= false
                        listener.invoke(pedidoSlected)
                    }
                }
                break
            }

        }
        notifyDataSetChanged()
    }

    fun haySeleccion(): Boolean {

        for (item in items) {
            if(item.slected){
                return true
            }else{
                for (pedidoSlected in item.pedido) {
                    if(pedidoSlected.slected)
                        return true
                }
            }
        }
        return false
    }


    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: PedidoGroup, context: Context,  listener: (Any) -> Unit){
            view.tittle.text= item.tittle

            var adapter= AdapterListPedidos(item.pedido, context){
                listener.invoke(it)
            }

            view.selected.setOnClickListener{
                listener.invoke(item)
            }

            view.recycler.layoutManager= LinearLayoutManager(context)
            view.recycler.adapter = adapter
        }

    }
}