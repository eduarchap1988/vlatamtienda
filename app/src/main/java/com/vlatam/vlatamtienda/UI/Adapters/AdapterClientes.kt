package com.vlatam.vlatamtienda.UI.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatamtienda.Data.Local.ClienteOpcion
import com.vlatam.vlatamtienda.Data.Local.EntM
import com.vlatam.vlatamtienda.R
import kotlinx.android.synthetic.main.item_ent_m.view.*

class AdapterClientes (var items: MutableList<EntM>, val context: Context,
                       val listener: (Any) -> Unit): RecyclerView.Adapter<AdapterClientes.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_ent_m, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: EntM, listener: (Any) -> Unit){
            view.codigo.text= item.nom_com
            view.direccion.text= item.dir
            view.dni.text= item.cif
            view.telefono.text= item.tlf
            view.mail.text= item.eml

            view.ly_telefono.setOnClickListener { listener(ClienteOpcion(item, 1)) }
            view.ly_mail.setOnClickListener { listener(ClienteOpcion(item, 2)) }

            view.setOnClickListener { listener(item) }

        }
    }
}


