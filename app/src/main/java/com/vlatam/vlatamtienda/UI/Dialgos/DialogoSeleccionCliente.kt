package com.vlatam.vlatamtienda.UI.Dialgos

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Window
import com.vlatam.vlatamtienda.Data.Local.SearchResponse
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.UI.Adapters.CustomSpinnerAdapterClientes
import kotlinx.android.synthetic.main.dialogo_seleccion_mesa.*


class DialogoSeleccionCliente: DialogFragment() {
    lateinit var listClientes: MutableList<SearchResponse>
    private lateinit var listener: (SearchResponse) -> Unit

    companion object {
        fun newInstance(listCliente: MutableList<SearchResponse>, listener: (SearchResponse) -> Unit): DialogoSeleccionCliente {
            var dialogo= DialogoSeleccionCliente()
            dialogo.listClientes= listCliente
            dialogo.listener= listener
            return dialogo
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogo = super.onCreateDialog(savedInstanceState)
        dialogo.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialogo.setContentView(R.layout.dialogo_seleccion_mesa)
        dialogo.setCancelable(true)

        initView(dialogo)

        return dialogo
    }

    private fun initView(dialogo: Dialog) {
        dialogo.spinner_clientes.adapter=  CustomSpinnerAdapterClientes(context!!, listClientes)

        /*

         */
        dialogo.aceptar.setOnClickListener{
            listener.invoke(listClientes[dialogo.spinner_clientes.selectedItemPosition])
            dialogo.dismiss()
        }

        dialogo.cancelar.setOnClickListener{
            dialogo.dismiss()
        }
    }

}