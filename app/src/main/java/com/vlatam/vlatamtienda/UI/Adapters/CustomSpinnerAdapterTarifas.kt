package com.vlatam.vlatamtienda.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.vlatam.vlatamtienda.Data.Local.VtaTarG
import com.vlatam.vlatamtienda.R
import kotlinx.android.synthetic.main.view_simple_spinner_tarifa.view.*

class CustomSpinnerAdapterTarifas (val context: Context, var listItemsTxt: List<VtaTarG>) : BaseAdapter() {


    val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View

        if (convertView == null) {
            view = mInflater.inflate(R.layout.view_simple_spinner_tarifa, parent, false)
        } else {
            view = convertView
        }

        view.codigo.text = listItemsTxt[position].name

        return view
    }
    fun getPositionItem(item: Int): Int {
        listItemsTxt.forEachIndexed { index, vtaTarG ->
            if(vtaTarG.id==item){
                return index
            }
        }
        return -1
    }

    override fun getItem(position: Int): VtaTarG {
        return listItemsTxt[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }


    override fun getCount(): Int {
        return listItemsTxt.size
    }

}