package com.vlatam.vlatamtienda.UI.Fragment

import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatamtienda.Data.Local.ClienteOpcion
import com.vlatam.vlatamtienda.Data.Local.EntM
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.UI.Adapters.AdapterClientes
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import kotlinx.android.synthetic.main.fragment_list_client.*


class FragmentClientes : Fragment(){

    lateinit var list : List<EntM>
    lateinit var listener : (EntM) -> Unit
    lateinit var adapter: AdapterClientes

    companion object {
        fun newInstance(searchList: List<EntM>, listener: (EntM) -> Unit): FragmentClientes {
            var fragment= FragmentClientes()
            fragment.list= searchList
            fragment.listener= listener
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_list_client, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler_clientes.layoutManager= LinearLayoutManager(context)
        recycler_clientes.scheduleLayoutAnimation()

        setuAdapter(list as MutableList<EntM>)

    }

    private fun setuAdapter(lista_art: MutableList<EntM>){

        adapter= AdapterClientes(lista_art, VlatamTiendaApplication.instance.getContext()) {
            if(it is ClienteOpcion){
                when(it.opcion){
                    1->{

                    }
                    2->{

                    }
                }
            }else
                if(it is EntM)
                    listener.invoke(it)
        }

        recycler_clientes.adapter = adapter
    }

    fun query(newQuery: String) {
        Log.e("TAG", "aplicar query ${newQuery}")
        var timer= object : CountDownTimer(1000, 1000) {
            override fun onTick(l: Long) {

            }

            override fun onFinish() {
                query(newQuery)
            }
        }


        try{
            if(newQuery.isEmpty()) {
                setuAdapter(list as MutableList<EntM>)
            }else{
                var lista_query= arrayListOf<EntM>()
                list.forEach {
                    if(it.nom_com.toLowerCase().contains(newQuery)){
                        lista_query.add(it)
                    }
                }
                setuAdapter(lista_query)
            }

            timer.cancel()

        }catch (e: Exception){
            Log.e("TAG", "error al aplicar query ${e.message}")
            timer.cancel()
            timer.start()

        }

    }



}