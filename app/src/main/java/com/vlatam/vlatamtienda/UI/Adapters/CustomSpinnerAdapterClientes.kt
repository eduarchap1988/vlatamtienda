package com.vlatam.vlatamtienda.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.vlatam.vlatamtienda.Data.Local.ClienteDireccion
import com.vlatam.vlatamtienda.Data.Local.SearchResponse
import com.vlatam.vlatamtienda.R
import kotlinx.android.synthetic.main.view_simple_spinner.view.*

class CustomSpinnerAdapterClientes (val context: Context, var listItemsTxt: MutableList<SearchResponse>) : BaseAdapter() {


    val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View

        if (convertView == null) {
            view = mInflater.inflate(R.layout.view_simple_spinner, parent, false)
        } else {
            view = convertView
        }

        view.codigo.text = listItemsTxt[position].name

        if(listItemsTxt[position].obj is ClienteDireccion){

            var item: ClienteDireccion = listItemsTxt[position].obj as ClienteDireccion

            if(item.direccion != null){

                view.direccion.text= item.direccion!!.dir
                view.direccion.visibility=View.VISIBLE
            }else
                view.direccion.visibility=View.GONE

        }


        return view
    }

    override fun getItem(position: Int): SearchResponse {
        return listItemsTxt[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }


    override fun getCount(): Int {
        return listItemsTxt.size
    }

}