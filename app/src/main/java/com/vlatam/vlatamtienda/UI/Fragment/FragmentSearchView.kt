package com.vlatam.vlatamtienda.UI.Fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatamtienda.Data.Local.SearchResponse
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.UI.Adapters.AdapterSearchView
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import kotlinx.android.synthetic.main.fragment_search_view.*


class FragmentSearchView : Fragment(){

    lateinit var list : MutableList<SearchResponse>
    lateinit var listener : (SearchResponse) -> Unit
    lateinit var adapter: AdapterSearchView

    companion object {
        fun newInstance(searchList: MutableList<SearchResponse>, listener: (SearchResponse) -> Unit): FragmentSearchView {
            var fragment= FragmentSearchView()
            fragment.list= searchList
            fragment.listener= listener
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_search_view, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter= AdapterSearchView(list, VlatamTiendaApplication.instance.getContext()) {
            listener.invoke(it)
        }
        recycler_search_view.layoutManager= LinearLayoutManager(context)
        recycler_search_view.adapter = adapter
        recycler_search_view.scheduleLayoutAnimation()

    }

}