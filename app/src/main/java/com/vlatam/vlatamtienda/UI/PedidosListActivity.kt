package com.vlatam.vlatamtienda.UI

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.vlatam.vlatamtienda.Helper.Status
import com.vlatam.vlatamtienda.Data.Local.Pedido
import com.vlatam.vlatamtienda.Data.Local.PedidoGroup
import com.vlatam.vlatamtienda.Data.Local.PedidoSlected
import com.vlatam.vlatamtienda.Listener.ListenerSincronizar
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.UI.Adapters.AdapterGroupListPedidos
import com.vlatam.vlatamtienda.UI.Adapters.AdapterListPedidos
import com.vlatam.vlatamtienda.UI.Dialgos.DialogoSync
import com.vlatam.vlatamtienda.ViewModels.PedidosListViewModel
import kotlinx.android.synthetic.main.activity_pedidos_list.*
import org.koin.android.viewmodel.ext.android.viewModel
import android.app.Activity
import com.google.gson.Gson
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class PedidosListActivity : AppCompatActivity(), ListenerSincronizar {

    private val viewModel: PedidosListViewModel by viewModel()
    private var adapter_pendientes: AdapterListPedidos? = null
    private var adapter_sync: AdapterGroupListPedidos? = null
    private var selected_p: Boolean= false


    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, PedidosListActivity::class.java)
            //intent.putExtra(INTENT_USER_ID, user.id)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pedidos_list)

        setupView()
        setupListPedidos()
        viewModel.updateListPedido()
        viewModel.updateListSincronizados()
    }


    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))

    }


    @SuppressLint("RestrictedApi")
    private fun setupListPedidos() {
        viewModel.pedidos_pendientes.observe(this, Observer {
            when(it!!.status){
                Status.SUCCESS->{
                    setupRecyclerPendientes(it!!.data)
                }
                Status.LOADING->{

                }
            }


        })


        viewModel.pedidos_sincronizados.observe(this, Observer {
            when(it!!.status){
                Status.SUCCESS->{
                    setupRecyclerSincronizados(it!!.data)
                }
                Status.LOADING->{

                }
            }


        })

        viewModel.pedidos_selected.observe(this, Observer {
            when(it!!.status){
                Status.SUCCESS->{
                    fab.visibility= View.VISIBLE
                }

                Status.ERROR->{
                    fab.visibility= View.GONE
                }

                Status.LOADING->{

                }
            }


        })

    }



    private fun setupRecyclerPendientes(data: MutableList<PedidoSlected>?) {
        if(data!!.isEmpty()){
            constraint_pendientes.visibility= View.GONE
            return
        }
        adapter_pendientes= AdapterListPedidos(data!!, this){
            if(it is PedidoSlected)
                viewModel.selectedPedido(it)
            else
                if(it is Pedido){
                   selectedPedido(it)
                }

        }

        recycler_pendientes.adapter = adapter_pendientes
    }

    private fun selectedPedido(it: Pedido) {
        val returnIntent = Intent()
        returnIntent.putExtra("data", Gson().toJson(it))
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }


    private fun setupRecyclerSincronizados(data: MutableList<PedidoGroup>?) {
        if(data!!.isEmpty()){
            constraint_sync.visibility= View.GONE
            return
        }

        adapter_sync= AdapterGroupListPedidos(data!!, this){
            if(it is PedidoSlected)
                viewModel.selectedPedido(it)
            else
            if(it is PedidoGroup){
                adapter_sync!!.selectedAll(it)
            }else
                if(it is Pedido){
                    selectedPedido(it)
                }
        }

        recycler_sincronizados.adapter = adapter_sync
    }
    override fun sincronizarPedido(pedido: Pedido) {


    }

    @SuppressLint("ResourceAsColor", "RestrictedApi")
    private fun setupView() {
        toolbar.title="Lista de Pedidos"
        //toolbar.setTitleTextColor(R.color.hint_color)
        toolbar.setNavigationIcon(R.drawable.arrow_left)
        toolbar.setNavigationOnClickListener {onBackPressed()}
        fab.visibility= View.GONE

        recycler_pendientes.layoutManager= LinearLayoutManager(this)
        recycler_sincronizados.layoutManager= LinearLayoutManager(this)


        selected_pendientes.setOnClickListener{
            if(!selected_p) {
                selected_p= true
                adapter_pendientes!!.selectedAll()
                viewModel.selectedPedientes()
            }else{
                selected_p= false
                adapter_pendientes!!.unselectedAll()
                viewModel.unselectedPedientes()
            }
        }


        fab.setOnClickListener{
            var hay_seleccion= false
            var hay_seleccion_p= false

            if(adapter_sync!=null){
                if(adapter_sync!!.haySeleccion())
                    hay_seleccion=true
            }

            if(adapter_pendientes!=null){
                if(adapter_pendientes!!.haySeleccion())
                    hay_seleccion_p=true
            }

            if(hay_seleccion || hay_seleccion_p){
                var dialogo = DialogoSync.newInstance(hay_seleccion_p){op ->
                    if(op){
                        viewModel.sincronizar()
                        /*val returnIntent = Intent()
                        setResult(Activity.RESULT_CANCELED, returnIntent)
                        finish()*/
                        val returnIntent = Intent()
                        returnIntent.putExtra("data", "inicio")
                        setResult(Activity.RESULT_OK, returnIntent)
                        finish()
                    }
                }
                dialogo.show(supportFragmentManager,"my_fragment")
            }
        }

    }
}
