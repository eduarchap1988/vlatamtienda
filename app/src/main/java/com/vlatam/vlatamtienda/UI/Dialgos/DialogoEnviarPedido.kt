package com.vlatam.vlatamtienda.UI.Dialgos

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Window
import com.vlatam.vlatamtienda.Data.Local.Pedido
import com.vlatam.vlatamtienda.R
import kotlinx.android.synthetic.main.dialogo_enviar_pedido.*


class DialogoEnviarPedido: DialogFragment() {
    private lateinit var listener: (Pedido) -> Unit
    private lateinit var pedido: Pedido
    companion object {
        fun newInstance(pedido: Pedido, listener: (Pedido) -> Unit): DialogoEnviarPedido {
            var dialogo= DialogoEnviarPedido()
            dialogo.listener= listener
            dialogo.pedido= pedido
            return dialogo
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogo = super.onCreateDialog(savedInstanceState)
        dialogo.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialogo.setContentView(R.layout.dialogo_enviar_pedido)
        dialogo.setCancelable(true)

        initView(dialogo)

        return dialogo
    }

    private fun initView(dialogo: Dialog) {

        dialogo.aceptar.setOnClickListener{
            pedido.obs= dialogo.edit_desc.text.toString()
            listener(pedido)
            dialogo.dismiss()
        }

        dialogo.cancelar.setOnClickListener{
            dialogo.dismiss()
        }


    }

}