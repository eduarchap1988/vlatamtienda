package com.vlatam.vlatamtienda.UI.Fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatamtienda.Data.Local.EntM
import com.vlatam.vlatamtienda.Data.Repository.CtaCorT
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.UI.Adapters.AdapterFacturas
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import kotlinx.android.synthetic.main.fragment_detalles_cliente.*
import kotlinx.android.synthetic.main.item_ent_m.*


class FragmentDetalleCliente : Fragment(){

    lateinit var list : List<CtaCorT>
    lateinit var cliente : EntM
    lateinit var listener : (Any) -> Unit
    lateinit var adapter: AdapterFacturas

    companion object {
        fun newInstance(cliente: EntM, searchList: List<CtaCorT>, listener: (Any) -> Unit): FragmentDetalleCliente {
            var fragment= FragmentDetalleCliente()
            fragment.list= searchList
            fragment.cliente=  cliente
            fragment.listener= listener
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_detalles_cliente, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        codigo.text= cliente.nom_com
        direccion.text= cliente.dir
        dni.text= cliente.cif
        telefono.text= cliente.tlf
        mail.text= cliente.eml

        ly_telefono.setOnClickListener {  }
        ly_mail.setOnClickListener {  }

        adapter= AdapterFacturas(list, VlatamTiendaApplication.instance.getContext()) {

        }

        recycler_facturas.layoutManager= LinearLayoutManager(context)
        recycler_facturas.adapter = adapter
        recycler_facturas.scheduleLayoutAnimation()

    }

}