package com.vlatam.vlatamtienda.UI

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.*
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.GravityCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import com.vlatam.vlatamtienda.Helper.Status
import com.google.gson.Gson
import com.vlatam.vlatamtienda.Data.Local.*
import com.vlatam.vlatamtienda.Listener.ListenerSincronizar
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.Servicios.ServicioDescarga
import com.vlatam.vlatamtienda.Servicios.ServicioPedidos
import com.vlatam.vlatamtienda.UI.Adapters.AdapterPedidos
import com.vlatam.vlatamtienda.UI.Dialgos.*
import com.vlatam.vlatamtienda.UI.Fragment.FragmentClientes
import com.vlatam.vlatamtienda.UI.Fragment.FragmentDetalleCliente
import com.vlatam.vlatamtienda.UI.Fragment.FragmentDetallePedido
import com.vlatam.vlatamtienda.UI.Fragment.FragmentSearchView
import com.vlatam.vlatamtienda.ViewModels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.navigation_view.*
import org.koin.android.viewmodel.ext.android.viewModel
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity(), ListenerSincronizar {

    private var adapterPedidos: AdapterPedidos? = null
    private var buscar_art: Boolean= false
    private var POS: Int=-1
    private var estado: Int=0
    private val mainViewModel: MainViewModel by viewModel()

    private var solicitud_share: Boolean= false
    private var solicitud_print: Boolean= false


    private var fragment_detalle=  FragmentDetallePedido.newInstance(Pedido(), arrayListOf(), null, listener = {
        //mainViewModel.add(it.obj)
        funcionPedido(it)
    })

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
        permisos()
        setupDataUser()
        setupListPedidos()
        setupPedido()
        setupListArticulos()
        setupCliente()
        setupListSearch()
        setupSearchSwitch()
        hideProgress()
        listenerBroadcast()
        listenerBroadcastDescarga()
        setupFileShared()
        setupClientes()
        setupFacturasCliente()
        mainViewModel.checkDatosInicio()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))

    }

    private fun permisos() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    2)
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CAMERA),
                    3)
        }


    }

    private fun listenerBroadcast() {
        val broadcastManager = LocalBroadcastManager.getInstance(this)
        val filter = IntentFilter(ServicioPedidos.EVENT_COUNT_FINISHED)

        filter.addAction(ServicioPedidos.EXTRA_SERVICE_STATUS)
        broadcastManager.registerReceiver(
                object : BroadcastReceiver() {
                    override fun onReceive(context: Context, intent: Intent) {
                        val statusService = intent.getStringExtra(ServicioPedidos.EXTRA_SERVICE_STATUS)
                        showToast(statusService)
                        Log.e("TAG", "STATUS SERVICIO $statusService")

                        if(statusService == getString(R.string.sync_exitosamente)){
                            ly_mensaje.visibility= View.GONE
                            //mainViewModel.stopServicio()
                            mainViewModel.updateListPedido()
                        }else
                            if(statusService == getString(R.string.error_api)){
                                ly_mensaje.visibility= View.GONE
                            }

                    }
                },
                filter)
    }



    private fun listenerBroadcastDescarga() {
        val broadcastManager = LocalBroadcastManager.getInstance(this)
        val filter = IntentFilter(ServicioDescarga.EVENT_COUNT_FINISHED)

        filter.addAction(ServicioDescarga.EXTRA_SERVICE_STATUS)
        broadcastManager.registerReceiver(
                object : BroadcastReceiver() {
                    override fun onReceive(context: Context, intent: Intent) {
                        val statusService = intent.getStringExtra(ServicioDescarga.EXTRA_SERVICE_STATUS)
                        showToast(statusService)
                        Log.e("TAG", "STATUS SERVICIO $statusService")

                        if(statusService == getString(R.string.sync_exitosamente)){
                            ly_mensaje.visibility= View.GONE
                            hideProgress()
                            mainViewModel.updateListPedido()
                        }else
                            if(statusService == getString(R.string.servicio_iniciado)){
                                ly_mensaje.visibility= View.VISIBLE
                            }else
                            if(statusService == getString(R.string.error_api)){
                                ly_mensaje.visibility= View.GONE
                                hideProgress()
                            }

                    }
                },
                filter)
    }

    override fun onStart() {
        super.onStart()
        //mainViewModel.prepareLocation()
    }

    private fun setupSearchSwitch() {
        if(estado==1) {
            floating_search_view.setSearchHint(getString(R.string.buscar_cliente))
            return
        }

        if(buscar_art)
            floating_search_view.setSearchHint(getString(R.string.buscar_articulo))
        else
            floating_search_view.setSearchHint(getString(R.string.buscar_cliente))
    }
    private fun setupFileShared() {
        mainViewModel.file_d.observe(this, Observer {
            animation_view2.visibility= View.GONE

            when(it!!.status){
                Status.SUCCESS->{

                    if (it.data!!.exists()) {
                        /*var uri =  FileProvider.getUriForFile(applicationContext, applicationContext.applicationContext.packageName + ".com.vlatam.vlatamtienda.provider",  it.data!!)
                        var share = Intent()
                        share.action = Intent.ACTION_SEND
                        share.putExtra(Intent.EXTRA_STREAM, uri)
                        startActivity(share)*/
                        //var uri = Uri.fromFile(it.data!!)
                        var uri =  FileProvider.getUriForFile(applicationContext, applicationContext.packageName + ".com.vlatam.vlatamtienda.provider",  it.data!!)
                        var i = Intent(Intent.ACTION_SEND)
                        i.type = "application/pdf"
                        i.putExtra(Intent.EXTRA_STREAM,  uri)
                        startActivity(Intent.createChooser(i, "Enviar mediante:"))
                    }
                }
                Status.LOADING->{
                    if (it.data!!.exists()) {

                        //var uri = Uri.fromFile(it.data!!)
                        var intent = Intent(Intent.ACTION_VIEW)
                        intent.setDataAndType(
                                FileProvider.getUriForFile(applicationContext, applicationContext.packageName + ".com.vlatam.vlatamtienda.provider", it.data!!),
                                "application/pdf")
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        startActivity(intent)
                    }
                }

            }

        })


    }

    private fun setupDataUser() {

        mainViewModel.user_data.observe(this, Observer{
            when (it!!.status) {
                Status.SUCCESS -> {

                    if(!it.data!!["img"]!!.isEmpty()){
                        val decodedString = Base64.decode(it.data!!["img"]!!, Base64.DEFAULT)
                        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                        image.setImageBitmap(decodedByte)
                    }

                    if(!it.data!!["nom_com"]!!.isEmpty())
                        user_name.text=it.data!!["nom_com"]!!

                }
                Status.ERROR -> {
                    val intent = LoginActivity.newIntent(this)
                    //intent.putExtra("nombre", )
                    //intent.putExtra("img", )
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                }
                Status.LOADING -> {
                }
            }
        })

    }

    private fun setupFacturasCliente() {
        mainViewModel.facturas.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    hideProgress()
                    showFragmentFacturas(it.data!!)

                }
                Status.ERROR -> {
                    Snackbar.make(fab, it.message!!, Snackbar.LENGTH_LONG).show()
                }
                Status.LOADING -> {
                }
            }
        })


    }

    private fun showFragmentFacturas(data: ClienteFacturas) {
        estado=2

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, FragmentDetalleCliente.newInstance(data.cliente, data.facturas){

                },
                        "fragmentfacturas")
                .commit()
    }

    private fun setupClientes() {

        mainViewModel.list_clientes.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    hideProgress()
                    showFragmentCliente(it.data!!)

                }
                Status.ERROR -> {
                    Snackbar.make(fab, it.message!!, Snackbar.LENGTH_LONG).show()
                }
                Status.LOADING -> {
                }
            }
        })


    }


    private lateinit var fragment_clientes: FragmentClientes

    @SuppressLint("RestrictedApi")
    private fun showFragmentCliente(data: List<EntM>) {
        fab.visibility=View.GONE
        estado=1
        setupSearchSwitch()

        fragment_clientes= FragmentClientes.newInstance(data){ent->
            showProgress()
            mainViewModel.selectedCliente(ent)
        }

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, fragment_clientes,
                        "fragmentarticulos")
                .commit()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 132) {
            if(resultCode == Activity.RESULT_OK){
                var result= data!!.getStringExtra("data")

                Log.e("TAG", "resilt OK $result")
                if(!result.isEmpty()) {
                    if(result=="inicio"){
                        ly_mensaje.visibility= View.VISIBLE
                    }else
                        mainViewModel.selectedPedido(Gson().fromJson(result, Pedido::class.java))
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                mainViewModel.nuevoPedido()
            }
        }else
        if (requestCode == 123) {
            if(resultCode == Activity.RESULT_OK){
                var result= data!!.getStringExtra("data")
                Log.e("TAG", "resilt OK $result")
                mainViewModel.nuevoPedidoQr(result!!)
            }

        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            2 -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] === PackageManager.PERMISSION_GRANTED) {
                    if(solicitud_print)
                        imprimir()
                    else
                        if(solicitud_share)
                            share_document()
                } // permiso denegado
                return
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private var is_search: Boolean=false

    override fun onBackPressed() {

        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {

            if(estado==2){
                mainViewModel.getListClientes()
                return
            }
            if(estado==1){
                mainViewModel.nuevoPedido()
                return
            }
            if(is_search){
                is_search=false
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame_layout, fragment_detalle,
                                "fragmentarticulos")
                        .commit()
                return
            }

            var dialogo = DialogoSalir.newInstance { valor->
                if(valor){
                    super.onBackPressed()
                }
            }
            dialogo.show(supportFragmentManager,"my_fragment")
        }
    }


    private fun setupListPedidos() {

        mainViewModel.pedidos.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    //userAdapter.addAllUsers(it.data!!)
                    POS=-1
                    setupAdapter(it.data!!)
                    hideProgressPedidos()
                    hideProgress()
                }
                Status.ERROR -> {
                    hideProgressPedidos()
                    hideProgress()
                    showToast(it.message!!)
                }
                Status.LOADING -> {
                    showProgress()
                    showProgressPedidos()
                }
            }
        })

    }

    @SuppressLint("RestrictedApi")
    private fun setupPedido() {
        mainViewModel.pedido.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    hideProgress()
                    setupAdapterArticulosPedido(it.data!!)
                    if(adapterPedidos!=null)
                        adapterPedidos!!.update(it.data!!)
                }
                Status.ERROR -> {
                    hideProgress()
                    ly_mensaje.visibility= View.GONE

                    when(it.message){
                        "UPDATE"->{
                            adapterPedidos!!.update(it.data!!)
                        }
                        "UPDATE VISTA"->{
                        }
                        "2"->{
                            Toast.makeText(this, "Pedido de solo lectura.", Toast.LENGTH_LONG).show()
                            //showToast("Pedido de solo lectura.")
                        }
                        "3"->{
                            showProgress()
                            showToast("Espere mientra capturamos su posicion.")
                        }
                        "4"->{
                            buscar_art= false
                            setupSearchSwitch()
                            fab.visibility= View.VISIBLE
                            menu_fab.visibility= View.GONE
                        }
                        else -> showToast(it.message!!)
                    }
                }
                Status.LOADING -> {
                }
            }
        })

    }

    private fun setupCliente() {
        mainViewModel.cliente.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    setupAdapterArticulosCliente(it.data!!)
                    buscar_art=true
                    setupSearchSwitch()
                }
                Status.ERROR -> {
                    if(it.message.equals("1")){ // se iba almacenar un pedido_selected sin un cliente
                        Snackbar.make(fab, getString(R.string.seleccione_cliente), Snackbar.LENGTH_LONG).show()
                    }else if(it.message.equals("2")){
                        showSettingsAlert()
                    } else if(it.message.equals("3")){

                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(this,
                                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                    555)
                        }
                    }
                    else{
                        setupAdapterArticulosCliente(null)
                    }
                }
                Status.LOADING -> {
                }
            }
        })

    }

    private fun setupListArticulos() {
        mainViewModel.artMList.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    //userAdapter.addAllUsers(it.data!!)
                    Log.e("TAG", "articulos SUCCES")
                    setupAdapterArticulos(it.data!!)
                }
                Status.ERROR -> {
                    Log.e("TAG", "articulo ERROR")
                    setupAdapterArticulos(arrayListOf())
                }
                Status.LOADING -> {
                    Log.e("TAG", "articulos LOADING")
                }
            }
        })
    }

    private fun setupListSearch() {

        mainViewModel.searchList.observe(this, Observer {
            when (it!!.status) {
                Status.SUCCESS -> {
                    //userAdapter.addAllUsers(it.data!!)
                    is_search=true
                    //Log.e("TAG", "search SUCCES")
                    setupAdapterSearch(it.data!!)
                }
                Status.ERROR -> {

                }

                Status.LOADING -> {

                   var dialogo = DialogoSeleccionCliente.newInstance(it.data!!) {
                       mainViewModel.nuevoPedidoQr(it)
                   }
                   dialogo.show(supportFragmentManager,"my_fragment")

                }
            }
        })

    }

    private fun setupAdapterSearch(listSearchView: List<SearchResponse>) {
        estado=0
        var fragment=  FragmentSearchView.newInstance(listSearchView as MutableList<SearchResponse>, listener = {
            floating_search_view.clearQuery()
            //ocultarTeclado()
            mainViewModel.add(it.obj)


        })

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, fragment,
                        "fragmentpedido")
                .commit()

    }

    @SuppressLint("RestrictedApi")
    private fun setupAdapterArticulosPedido(pedido: Pedido) {
        if(!pedido.is_editable){
            fab.visibility=View.GONE
            menu_fab.visibility= View.VISIBLE
        }else{
            menu_fab.visibility= View.GONE
        }

        fragment_detalle.setTotal(pedido)

        /*
        fragment_detalle=  FragmentDetallePedido.newInstance(pedido, fragment_detalle.list, fragment_detalle.cliente, listener = {
            //mainViewModel.add(it.obj)
            funcionPedido(it)
        })

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, fragment_detalle,
                        "fragmentarticulos")
                .commit(*/
    }

    @SuppressLint("RestrictedApi")
    private fun setupAdapterArticulosCliente(cliente: ClienteDireccion?) {
        estado=0
        fragment_detalle=  FragmentDetallePedido.newInstance(fragment_detalle.pedido, fragment_detalle.list, cliente, listener = {
            //mainViewModel.add(it.obj)
            funcionPedido(it)
        })

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, fragment_detalle,
                        "fragmentarticulos")
                .commit()
    }

    private fun funcionPedido(it: Any) {
        if(it is PedidoArticulo){
            mainViewModel.update(it)
        }else
            if(it is ClienteDireccion){
                buscar_art = !buscar_art
                setupSearchSwitch()
            }else
                if(it is VtaTarG){
                    mainViewModel.selectedVtaTar(it)
                }
        //ocultarTeclado()
    }

    private fun setupAdapterArticulos(list: MutableList<PedidoArticulo>) {

        /*fragment_detalle=  FragmentDetallePedido.newInstance(fragment_detalle.pedido, list, fragment_detalle.cliente, listener = {
            //mainViewModel.add(it.obj)
            funcionPedido(it)
        })
        } supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, fragment_detalle,
                        "fragmentarticulos")
                .commit()*/
        estado=0
        fragment_detalle.setListaArticulos(list)

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, fragment_detalle,
                        "fragmentarticulos")
                .commit()
    }


    private fun showSettingsAlert() {
        var dialogo : DialogoGps = DialogoGps.newInstance { valor->
            if(valor){
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                applicationContext.startActivity(intent)
            }
        }
        dialogo.show(supportFragmentManager,"my_fragment")

    }

    private fun setupAdapter(listPedidos: MutableList<Pedido>) {
        Log.e("TAG", "POS  $POS")

        adapterPedidos= AdapterPedidos(listPedidos, this, POS, this) {pedido->

            POS= adapterPedidos!!.getPos(pedido)
            drawer_layout.closeDrawer(GravityCompat.START)
            setupAdapter(listPedidos)

            mainViewModel.selectedPedido(pedido)
        }
        recycler_lista_pedidos.adapter = adapterPedidos
    }

    override fun sincronizarPedido(pedido: Pedido) {
        drawer_layout.closeDrawer(GravityCompat.START)

        var dialogo : DialogoEnviarPedido= DialogoEnviarPedido.newInstance(pedido) {
            showProgress()
            ly_mensaje.visibility= View.VISIBLE
            mainViewModel.sincronizar(pedido)
        }
        dialogo.show(supportFragmentManager,"my_fragment")


    }




    @SuppressLint("RestrictedApi")
    private fun setupView() {

        //toolbar.title=" "
        //toolbar.setNavigationIcon(R.drawable.menu)
        //toolbar.setNavigationOnClickListener {drawer_layout.openDrawer(GravityCompat.START)}
        floating_search_view.attachNavigationDrawerToMenuButton(drawer_layout)

        //fab.visibility= View.GONE


        recycler_lista_pedidos.layoutManager= LinearLayoutManager(this)

        nuevo_pedido.setOnClickListener {
            //mainViewModel.nuevoPedido()
            drawer_layout.closeDrawer(GravityCompat.START)

            if(fragment_detalle.list.size>0 || fragment_detalle.cliente!=null){
                var dialogo = DialogoNuevoPedido.newInstance{
                    POS=-1
                    adapterPedidos!!.pos= POS
                    adapterPedidos!!.notifyDataSetChanged()
                    mainViewModel.nuevoPedido()
                }
                dialogo.show(supportFragmentManager,"my_fragment")
            }
        }

        fab.setOnClickListener{
            mainViewModel.createNewPedido()
        }

        accion_share.setOnClickListener{
            share_document()

        }

        accion_imprimir.setOnClickListener{
            imprimir()

        }

        menu_fab.visibility= View.GONE

        floating_search_view.setOnQueryChangeListener { oldQuery, newQuery ->

            if(estado==1) {// filtrar en la lista de cliente
                fragment_clientes.query(newQuery.toLowerCase())
                return@setOnQueryChangeListener
            }else{
                if (buscar_art)
                    mainViewModel.doSearch(newQuery, 10)
                else
                    mainViewModel.doSearchClient(newQuery, 10)
            }
        }


        setings.setOnClickListener{
            createPopup(it)
        }


    }

    private fun share_document() {
        if(solicitud_share)
            solicitud_share= false
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    2)
            solicitud_share= true
        }else{
            showProgress()
            mainViewModel.sharePedido()
        }

    }

    private fun imprimir() {
        if(solicitud_print)
            solicitud_print= false

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    2)
            solicitud_print= true
        }else{
            showProgress()
            mainViewModel.imprimir()
        }
    }


    /*
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        val location = IntArray(2)
        setings.getLocationOnScreen(location)
         //Initialize the Point with x, and y positions
        p.x = location[0]
        p.y = location[1]

        Log.e("TAG", "POINT $p")

    }*/

    private fun createPopup(it: View) {
        val popup = PopupMenu(this, it)
        menuInflater.inflate(R.menu.main, popup.menu)

        try {
            var fields = popup.javaClass.declaredFields

            for (field in fields) {
               // Log.e("TAG", "field name  ${field.name}")
                if ("mPopup" == field.name) {
                    field.isAccessible = true
                    val menuPopupHelper = field.get(popup)
                    val classPopupHelper = Class.forName(menuPopupHelper.javaClass.name)
                    val setForceIcons = classPopupHelper.getMethod("setForceShowIcon", Boolean::class.java)
                    setForceIcons.invoke(menuPopupHelper, true)
                    break
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        popup.setOnMenuItemClickListener { item ->
            when(item.itemId){
                R.id.gestion_pedido->{
                    startActivityForResult(PedidosListActivity.newIntent(this), 132)
                }

                R.id.gestion_clientes->{
                    mainViewModel.getListClientes()
                }
                R.id.action_qr->{

                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(this,
                                arrayOf(Manifest.permission.CAMERA),
                                3)
                    }else
                        startActivityForResult(Intent(this, ScannerActivity::class.java), 123)
                }
                R.id.reload->{
                    showProgress()
                    showProgressPedidos()
                    ly_mensaje.visibility= View.VISIBLE
                    mainViewModel.reload()
                }
                R.id.cerrar_sesion->{
                    var dialogo = DialogoCerrarSesion.newInstance { valor->
                        if(valor){
                            mainViewModel.cerrarSesion()
                        }
                    }
                    dialogo.show(supportFragmentManager,"my_fragment")
                }
            }
            true
        }
        popup.show()
    }

    fun ocultarTeclado() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(constraintLayout.windowToken, 0)
    }
    private fun showToast(message: String) {
        val mSnackBar = Snackbar.make(drawer_layout, message, Snackbar.LENGTH_LONG)
        val mainTextView = mSnackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mainTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        } else {
            mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        }
        mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        mainTextView.setTextColor(resources.getColor(R.color.colorPrimary))
        mSnackBar.show()
    }

    fun showProgressPedidos() {
        animation_view.visibility = View.VISIBLE
        recycler_lista_pedidos.visibility = View.GONE
    }

    fun hideProgressPedidos() {
        animation_view.visibility= View.GONE
        recycler_lista_pedidos.visibility= View.VISIBLE
    }

    fun showProgress() {
        animation_view2.visibility= View.VISIBLE
    }

    fun hideProgress() {
        animation_view2.visibility= View.GONE
    }


}
