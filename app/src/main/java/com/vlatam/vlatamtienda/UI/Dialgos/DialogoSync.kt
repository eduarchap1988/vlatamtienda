package com.vlatam.vlatamtienda.UI.Dialgos

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Window
import com.vlatam.vlatamtienda.R
import kotlinx.android.synthetic.main.dialogo_sali.*


class DialogoSync: DialogFragment() {
    lateinit var listener: (Boolean) -> Unit
    var pendiente: Boolean = false

    companion object {
        fun newInstance(pendiente: Boolean, listener: (Boolean) -> Unit): DialogoSync {
            var dialogo= DialogoSync()
            dialogo.listener= listener
            dialogo.pendiente= pendiente
            return dialogo
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogo = super.onCreateDialog(savedInstanceState)
        dialogo.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialogo.setContentView(R.layout.dialogo_sali)
        dialogo.setCancelable(true)

        initView(dialogo)

        return dialogo
    }

    private fun initView(dialogo: Dialog) {
        dialogo.titulo.text= getString(R.string.alerta)
        if(pendiente)
            dialogo.mensaje.text= getString(R.string.mensaje_sync_2)
        else
            dialogo.mensaje.text= getString(R.string.mensaje_sync)

        dialogo.aceptar.text= getString(R.string.si)
        dialogo.cancelar.text= getString(R.string.no)

        dialogo.aceptar.setOnClickListener{
            listener(true)
            dialogo.dismiss()
        }

        dialogo.cancelar.setOnClickListener{
            listener(false)
            dialogo.dismiss()
        }


    }

}