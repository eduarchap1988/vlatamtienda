package com.vlatam.vlatamtienda.UI.Adapters

import android.content.Context
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatamtienda.Data.Repository.CtaCorT
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import kotlinx.android.synthetic.main.item_facturas.view.*

class AdapterFacturas (val items: List<CtaCorT>, val context: Context,
                       val listener: (Any) -> Unit): RecyclerView.Adapter<AdapterFacturas.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_facturas, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: CtaCorT, listener: (Any) -> Unit){
            view.codigo.text= item.num_doc
            view.fecha_vto.text= "Vto. ${item.fch_vto.substring(0, 10)}"
            view.fecha.text= "Emt. ${item.fch.substring(0, 10)}"

            view.monto.text= "Mnt. ${item.moneda}"+item.mnt_tot.toString()
            view.pago.text= "Pgo. ${item.moneda}"+item.mnt_cru.toString()
            view.pendiente.text= "Pdt. ${item.moneda}"+item.sal.toString()

            if(item.sal>0){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    view.pendiente.setTextColor(VlatamTiendaApplication.instance.getColor(R.color.colorRojo))
                }
            }
            view.setOnClickListener { listener(item) }

        }
    }
}


