package com.vlatam.vlatamtienda.UI.Adapters

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.vlatam.vlatamtienda.Data.Local.PedidoArticulo
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import com.vlatam.vlatmrest.Helper.Utils
import kotlinx.android.synthetic.main.item_art.view.*

class AdapterArticulos (val items: MutableList<PedidoArticulo>, val context: Context, val sincronizado: Boolean,
                        val listener: (PedidoArticulo) -> Unit): RecyclerView.Adapter<AdapterArticulos.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener, sincronizado)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_art, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: PedidoArticulo, listener: (PedidoArticulo) -> Unit, sincronizado: Boolean){
            if(item.art_m==null)
                return

            view.codigo.text= item.art_m!!.name
            view.price.text= "P.U. $ ${item.art_m!!.pvp.toString()}"
            view.cantidad.setText(Utils.obtieneDosDecimales(item.cantidad).replace(',', '.'))


            if(!sincronizado){

                view.cantidad.setOnFocusChangeListener{view, hasFocus->
                    if(!hasFocus){
                        if(!view.cantidad.text.toString().isEmpty()){
                            item.cantidad= view.cantidad.text.toString().toFloat()
                            listener(item)

                        }
                    }

                }

                view.cantidad.setOnKeyListener OnKeyListener@{ v, keyCode, event ->
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                            //aqui iria tu codigo al presionar el boton enter o done
                            if(!view.cantidad.text.toString().isEmpty()){
                                item.cantidad= view.cantidad.text.toString().toFloat()
                                listener(item)
                                ocultarTeclado(view)
                            }
                        }


                        return@OnKeyListener true
                    }
                    false
                }

            }else{
                view.cantidad.isEnabled= false

            }



        }


        fun ocultarTeclado(view: View) {
            val imm = VlatamTiendaApplication.instance.getContext().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}


