package com.vlatam.vlatamtienda.UI.Fragment

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatamtienda.Data.Local.ClienteDireccion
import com.vlatam.vlatamtienda.Data.Local.EntM
import com.vlatam.vlatamtienda.Data.Local.Pedido
import com.vlatam.vlatamtienda.Data.Local.PedidoArticulo
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.UI.Adapters.AdapterArticulos
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import kotlinx.android.synthetic.main.fragment_detalles_pedido.*
import android.support.v7.widget.helper.ItemTouchHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import com.vlatam.vlatamtienda.UI.Adapters.CustomSpinnerAdapterTarifas
import com.vlatam.vlatmrest.Helper.Utils


class FragmentDetallePedido: Fragment() {
    lateinit var list : MutableList<PedidoArticulo>
    lateinit var listener : (Any) -> Unit
    lateinit var adapter: AdapterArticulos
    var cliente: ClienteDireccion?= null
    lateinit var pedido : Pedido

    companion object {
        fun newInstance(pedido: Pedido, listArtm: MutableList<PedidoArticulo>, cliente: ClienteDireccion?, listener: (Any) -> Unit ): FragmentDetallePedido {
            var fragment= FragmentDetallePedido()
            fragment.list=  listArtm
            fragment.cliente= cliente
            fragment.pedido= pedido
            fragment.listener= listener
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_detalles_pedido, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter= AdapterArticulos(list, VlatamTiendaApplication.instance.getContext(), pedido.is_sincronizado) {
            listener.invoke(it)
        }

        recycler_artm.layoutManager= LinearLayoutManager(context)
        recycler_artm.adapter = adapter
        recycler_artm.scheduleLayoutAnimation()

        if(cliente!=null){
            name_client.text= cliente!!.cliente.nom_com

            if(!cliente!!.cliente.img.isEmpty()){
                val decodedString = Base64.decode(cliente!!.cliente.img, Base64.DEFAULT)
                val decodedByte: Bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                image.setImageBitmap(decodedByte)
            }

            if(cliente!!.direccion!=null){
                direccion.text=cliente!!.direccion!!.dir_etq

            }else
                direccion.visibility= View.GONE


            if(cliente!!.vta_tar_g.isNotEmpty()){
                var adarter_tarfias= CustomSpinnerAdapterTarifas(context!!, cliente!!.vta_tar_g)
                spinner_tarifas.adapter= adarter_tarfias
                var pos= adarter_tarfias.getPositionItem(cliente!!.cliente.vta_tar)
                if(pos!=-1)
                    spinner_tarifas.setSelection(pos)


                spinner_tarifas.onItemSelectedListener= object : OnItemSelectedListener{
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        Log.e("TAG", "selected ${cliente!!.vta_tar_g[spinner_tarifas.selectedItemPosition]}")
                        listener(cliente!!.vta_tar_g[spinner_tarifas.selectedItemPosition])
                    }
                }

            }else{
                spinner_tarifas.visibility= View.GONE
                text_tarifa.visibility= View.GONE
                img_tarifas.visibility= View.GONE
            }



        }else{
            name_client.text= ""
            direccion.text=""
            image.setImageResource(R.drawable.logo_circle)
            spinner_tarifas.visibility= View.GONE
            text_tarifa.visibility= View.GONE
            img_tarifas.visibility= View.GONE
        }

        if(list.isEmpty()){
            total_articulos.text= "Cnt.Art.\n0"
            total_pedido.text= "Total\n0"
        }else{
            total_articulos.text= "Cnt.Art.\n${Utils.obtieneDosDecimales(pedido.total_articulos)}"
            total_pedido.text= "Total\n${pedido.mon_sim} ${Utils.obtieneDosDecimales(pedido.total)}"
        }


        if(!pedido.is_sincronizado){
            val simpleItemTouchCallback = object : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN, ItemTouchHelper.RIGHT) {
                override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                    //awesome code when user grabs recycler card to reorder
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    //awesome code when swiping right to remove recycler card and delete SQLite data
                    Log.e("TAG", "on swipee " + viewHolder.adapterPosition)
                    if(!pedido.is_sincronizado) {
                        var art = adapter.items[viewHolder.adapterPosition]
                        art.cantidad = 0f
                        listener.invoke(art)
                    }else
                        adapter.notifyDataSetChanged()

                }
            }
            val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
            itemTouchHelper.attachToRecyclerView(recycler_artm)


            image.setOnClickListener{
                if(cliente!=null)
                    listener.invoke(cliente!!)
                else
                    listener.invoke(ClienteDireccion(EntM(), null))
            }

        }


    }


    fun setListaArticulos(lista : MutableList<PedidoArticulo>){
        try {
            this.list= lista

            adapter= AdapterArticulos(list, VlatamTiendaApplication.instance.getContext(), pedido.is_sincronizado) {
                listener.invoke(it)
            }
            recycler_artm.adapter = adapter
        }catch (e: Exception){

        }

    }

    fun setTotal(pedido_e: Pedido){
        try {
            this.pedido= pedido_e
            total_pedido.text= "Total\n${pedido.mon_sim} ${Utils.obtieneDosDecimales(pedido.total)}"
            total_articulos.text= "Cnt.Art.\n${Utils.obtieneDosDecimales(pedido.total_articulos)}"
        }catch (e: Exception){

        }

    }



}