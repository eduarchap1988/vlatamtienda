package com.vlatam.vlatamtienda.UI.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatamtienda.Data.Local.ArtM
import com.vlatam.vlatamtienda.Data.Local.ClienteDireccion
import com.vlatam.vlatamtienda.Data.Local.SearchResponse
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import kotlinx.android.synthetic.main.item_search.view.*

class AdapterSearchView (val items: List<SearchResponse>, val context: Context,
                         val listener: (SearchResponse) -> Unit): RecyclerView.Adapter<AdapterSearchView.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_search, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: SearchResponse, listener: (SearchResponse) -> Unit){
            view.codigo.text= item.name
            if(item.obj is ClienteDireccion){
                if(item.obj.direccion!=null){
                    view.direccion.text= item.obj.direccion!!.dir
                    view.direccion.visibility=View.VISIBLE
                }

            }else
                if(item.obj is ArtM && VlatamTiendaApplication.EXISTENCIA){
                    view.cant.text= item.obj.exs.toString()
                    view.cant.visibility= View.VISIBLE
                    view.direccion.text= "$ ${item.obj.pvp.toString()}"
                    view.direccion.visibility=View.VISIBLE
                }
            view.setOnClickListener {
                listener(item)
            }
        }

    }
}