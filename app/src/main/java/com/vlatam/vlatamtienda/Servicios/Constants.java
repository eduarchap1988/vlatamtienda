package com.vlatam.vlatamtienda.Servicios;

/**
 * Created by Eukaris on 17/08/2018.
 */

public class Constants {

    public interface ACTION {
        public static String MAIN_ACTION = " com.vlatam.vlatamtienda.foregroundservice.action.main";
        public static String STARTFOREGROUND_ACTION = " com.vlatam.vlatamtienda.foregroundservice.action.startforeground";
        public static String STOPFOREGROUND_ACTION = " com.vlatam.vlatamtienda.foregroundservice.action.stopforeground";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
        public static int FOREGROUND_SERVICE_DESCARGAR = 121;
    }
}
