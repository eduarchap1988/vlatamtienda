package com.vlatam.vlatamtienda.Servicios

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.preference.PreferenceManager
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.vlatam.vlatamtienda.Data.Local.*
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import okhttp3.*
import java.io.IOException
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import com.vlatam.vlatamtienda.UI.MainActivity
import android.graphics.Color
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationCompat.PRIORITY_MIN
import com.vlatam.vlatamtienda.Data.Repository.*


class ServicioDescarga : Service()   {
    companion object {
        val CANONICAL_NAME = ServicioDescarga::class.java!!.getCanonicalName()
        val EXTRA_SERVICE_STATUS = "Status_servicio_descarga"
        val EVENT_COUNT_FINISHED = CANONICAL_NAME!! + ".COUNT_FINISHED"
    }

    private val client= OkHttpClient()
    private val preferences= PrefrencesManager(PreferenceManager.getDefaultSharedPreferences(VlatamTiendaApplication.instance.getContext()))
    private val context= VlatamTiendaApplication.instance.getContext()

    private val daoPedidos = PedidoDao()
    private val daoClientes = ClienteDao()
    private val daoArticulos= ArticulosDao()
    private val daoDirectorio= DirectorioDao()
    private val daoTarifas= TarifasDao()
    private val daoPrecios= PreciosDao()
    private val daoPedidoArticulo= PedidoArticuloDao()
    private val daoTarifaCliente= TarifaClienteDao()
    private val daoMoneda= MonedaDao()

    override fun onCreate() {
        Log.i("TAG", "Service onCreate")
        //showMessage(getString(R.string.sync_inicio))
    }

    private lateinit var notification: Notification

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        Log.i("TAG", "Service onStartCommand $startId")

        showMessage(getString(R.string.servicio_iniciado))
        consumeArt_m(1, true)

        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.action = Constants.ACTION.MAIN_ACTION
        notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)


        // startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);
        //NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        var channelId = ""

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel("service_pedido", "vLatamTienda")
        }

        val icon = BitmapFactory.decodeResource(resources, R.drawable.logo_vlatam_tienda)
        //val icon = BitmapFactory.decodeResource(getResources(), android.R.drawable.ic_do);

        notification = NotificationCompat.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setTicker(getString(R.string.app_name))
                .setContentText(getString(R.string.descargardo_datos))
                .setSmallIcon(R.drawable.logo_circle)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setPriority(PRIORITY_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build()


        //notificationManager.notify(new Random().nextInt(), notification);
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE_DESCARGAR, notification)

        return Service.START_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE)

        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    override fun onBind(intent: Intent): IBinder? {
        Log.i("TAG", "Service onBind")
        return null
    }

    override fun onDestroy() {
        Log.i("TAG", "Service onDestroy")
    }



    private fun consumeArt_m(pag: Int, nextPage: Boolean) {

        var endpoint=preferences.getBasrUrl()+"v1/art_m?api_key=${preferences.getLoginInfo()!!.api_key}&filter[dis_app]=true&page[number]=$pag"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "consumeArt_m $endpoint")

        client.newCall(request).enqueue(object: Callback {
            @SuppressLint("NewApi")
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                stopForeground(Service.STOP_FOREGROUND_REMOVE)
                showMessage(context.getString(R.string.error_api)+" ${e.message}")
            }

            @SuppressLint("NewApi")
            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO art_m $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    showMessage("El API Key de la solicitud no es válido")
                    stopForeground(Service.STOP_FOREGROUND_REMOVE)
                }else
                    if (response.isEmpty() || response == "{}") {
                        showMessage(context.getString(R.string.error_api))
                        stopForeground(Service.STOP_FOREGROUND_REMOVE)
                    } else {

                        try {
                            var data : ResponseArtM = Gson().fromJson(response, ResponseArtM::class.java)

                                if(pag==1) {
                                    daoArticulos.removeAll()
                                }

                                data.art_m.forEach { x ->
                                    daoArticulos.add(x)
                                }

                                if(nextPage && data.count!=data.total_count){
                                    var div= data.total_count/data.count
                                    var resto= data.total_count%data.count
                                    if(resto>0)
                                        div++
                                    Log.e("TAG", "RESULTADO DE LA DIVISION $div ${pag<div}")
                                    if(pag<=div){
                                        val p= pag+1
                                        consumeArt_m(p, p<div)
                                    }
                                }else
                                    consumeEnt_m(1, true)


                        } catch (e: java.lang.Exception) {
                            Log.e("TAG", "ERROR EN consumeArt_m ${e.message}")
                            showMessage("ERROR EN consumeArt_m ${e.message}")
                            stopForeground(Service.STOP_FOREGROUND_REMOVE)
                        }

                    }


            }
        })

    }

    @SuppressLint("NewApi")
    private fun consumeEnt_m(pag: Int, nextPage: Boolean) {
        Log.e("TAG", "consumeEnt_m")

        var endpoint= preferences.getBasrUrl()+"v1/ent_m?filter[id_es_clt]=&api_key=${preferences.getLoginInfo()!!.api_key}&page[number]=$pag"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL $endpoint")

        client.newCall(request).enqueue(object: Callback {

            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                stopForeground(Service.STOP_FOREGROUND_REMOVE)
                showMessage(context.getString(R.string.error_api)+" ${e.message}")            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO ent_m $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    stopForeground(Service.STOP_FOREGROUND_REMOVE)
                    showMessage(context.getString(R.string.error_api))
                }else
                    if (response.isEmpty() || response == "{}") {
                        //message.postValue(context.getString(R.string.error_api))
                        stopForeground(Service.STOP_FOREGROUND_REMOVE)
                        showMessage(context.getString(R.string.error_api))
                    } else {

                        try {
                            var data : ResponseEntM = Gson().fromJson(response, ResponseEntM::class.java)

                                if(pag==1) {
                                    daoClientes.removeAll()
                                }

                                data.ent_m.forEach { x ->
                                    daoClientes.add(x)
                                }

                                if(nextPage && data.count!=data.total_count){
                                    var div= data.total_count/data.count
                                    var resto= data.total_count%data.count
                                    if(resto>0)
                                        div++
                                    Log.e("TAG", "RESULTADO DE LA DIVISION $div ${pag<div}")
                                    if(pag<=div){
                                        val p= pag+1
                                        consumeEnt_m(p, p<div)
                                    }
                                }else
                                    consumeDir_m(1, true)

                        } catch (e: java.lang.Exception) {
                            stopForeground(Service.STOP_FOREGROUND_REMOVE)
                            showMessage(context.getString(R.string.error_api))
                        }

                    }


            }
        })
    }


    @SuppressLint("NewApi")
    private fun consumeDir_m(pag: Int, nextPage: Boolean) {

        Log.e("TAG", "consumeDir_m")

        var endpoint= preferences.getBasrUrl()+"v1/dir_m?api_key=${preferences.getLoginInfo()!!.api_key}&page[number]=$pag"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL  $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                stopForeground(Service.STOP_FOREGROUND_REMOVE)
                showMessage(context.getString(R.string.error_api))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO dir_m $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    stopForeground(Service.STOP_FOREGROUND_REMOVE)
                    showMessage(context.getString(R.string.error_api))
                }else
                    if (response.isEmpty() || response == "{}") {
                        stopForeground(Service.STOP_FOREGROUND_REMOVE)
                        showMessage(context.getString(R.string.error_api))
                    } else {

                        try {
                            var data : ResponseDirM = Gson().fromJson(response, ResponseDirM::class.java)


                                if(pag==1) {
                                    daoDirectorio.removeAll()
                                }


                                data.dir_m.forEach {x ->
                                    daoDirectorio.add(x)
                                }


                                if(nextPage && data.count!=data.total_count){
                                    var div= data.total_count/data.count
                                    var resto= data.total_count%data.count
                                    if(resto>0)
                                        div++
                                    Log.e("TAG", "RESULTADO DE LA DIVISION $div ${pag<div}")
                                    if(pag<=div){
                                        val p= pag+1
                                        consumeDir_m(p, p<div)
                                    }
                                }else
                                   consumePrecios(1, true)


                        } catch (e: java.lang.Exception) {
                            stopForeground(Service.STOP_FOREGROUND_REMOVE)
                            showMessage(context.getString(R.string.error_api))
                        }

                    }


            }
        })
    }

    @SuppressLint("NewApi")
    private fun consumePrecios(pag: Int, nextPage: Boolean) {
        Log.e("TAG", "consumePrecios")
        var endpoint= preferences.getBasrUrl()+"v1/vta_tar_art_g?api_key=${preferences.getLoginInfo()!!.api_key}&page[number]=$pag"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL  $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                stopForeground(Service.STOP_FOREGROUND_REMOVE)
                showMessage(context.getString(R.string.error_api))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO consumePrecios $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    stopForeground(Service.STOP_FOREGROUND_REMOVE)
                    showMessage(context.getString(R.string.error_api))
                }else
                    if (response.isEmpty() || response == "{}") {
                        stopForeground(Service.STOP_FOREGROUND_REMOVE)
                        showMessage(context.getString(R.string.error_api))
                    } else {

                        try {
                            var data : ResponseVtaTarArtG = Gson().fromJson(response, ResponseVtaTarArtG::class.java)



                                if (pag == 1) {
                                    daoPrecios.removeAll()
                                }

                                data.vta_tar_art_g.forEach { x ->
                                    daoPrecios.add(x)
                                }

                                if(nextPage && data.count!=data.total_count){
                                    var div = data.total_count / data.count
                                    var resto = data.total_count % data.count
                                    if (resto > 0)
                                        div++
                                    Log.e("TAG", "RESULTADO DE LA DIVISION $div ${pag < div}")
                                    if (pag <= div) {
                                        val p = pag + 1
                                        consumePrecios(p, p < div)
                                    }
                                } else
                                    consumeTarifaCliente(1, true)

                        } catch (e: java.lang.Exception) {
                            stopForeground(Service.STOP_FOREGROUND_REMOVE)
                            showMessage(context.getString(R.string.error_api))
                        }

                    }
            }
        })
    }

    @SuppressLint("NewApi")
    private fun consumeTarifaCliente(pag: Int, nextPage: Boolean) {


        var endpoint= preferences.getBasrUrl()+"v1/vta_tar_cli_g?api_key=${preferences.getLoginInfo()!!.api_key}&page[number]=$pag"
        Log.e("TAG", "endpoint $endpoint")

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL  $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                stopForeground(Service.STOP_FOREGROUND_REMOVE)
                showMessage(context.getString(R.string.error_api))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "consumeTarifaCliente $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    stopForeground(Service.STOP_FOREGROUND_REMOVE)
                    showMessage(context.getString(R.string.error_api))
                }else
                    if (response.isEmpty() || response == "{}") {
                        stopForeground(Service.STOP_FOREGROUND_REMOVE)
                        showMessage(context.getString(R.string.error_api))
                    } else {

                        try {
                            var data : ResponseVtaTarCliG = Gson().fromJson(response, ResponseVtaTarCliG::class.java)

                                if (pag == 1) {
                                    daoTarifaCliente.removeAll()
                                }

                                data.vta_tar_cli_g.forEach {x ->
                                    daoTarifaCliente.add(x)
                                }

                                if(nextPage && data.count!=data.total_count){
                                    var div = data.total_count / data.count
                                    var resto = data.total_count % data.count
                                    if (resto > 0)
                                        div++
                                    Log.e("TAG", "RESULTADO DE LA DIVISION $div ${pag < div}")
                                    if (pag <= div) {
                                        val p = pag + 1
                                        consumeTarifaCliente(p, p < div)
                                    }
                                }else
                                    consumeMon_c(1, true)

                        } catch (e: java.lang.Exception) {
                            stopForeground(Service.STOP_FOREGROUND_REMOVE)
                            showMessage(context.getString(R.string.error_api))
                        }

                    }
            }
        })
    }


    @SuppressLint("NewApi")
    private fun consumeMon_c(pag: Int, nextPage: Boolean) {

        //ttp://tienda.vlatamweb.com/API/vLatamERP_db_dat/v1/mon_c?api_key=tienda123
        var endpoint= preferences.getBasrUrl()+"v1/mon_c?api_key=${preferences.getLoginInfo()!!.api_key}&page[number]=$pag"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                stopForeground(Service.STOP_FOREGROUND_REMOVE)
                showMessage(context.getString(R.string.error_api))
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "RSULTADO mon_c $response")

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    stopForeground(Service.STOP_FOREGROUND_REMOVE)
                    showMessage(context.getString(R.string.error_api))
                }else
                    if (response.isEmpty() || response == "{}") {
                        stopForeground(Service.STOP_FOREGROUND_REMOVE)
                        showMessage(context.getString(R.string.error_api))

                    } else {

                        try {
                            var data = Gson().fromJson(response, ResponseMonC::class.java)
                            if (pag == 1) {
                                daoMoneda.removeAll()
                            }

                            data.mon_c.forEach { x ->
                                daoMoneda.add(x)
                            }

                            if(nextPage && data.count!=data.total_count){
                                var div = data.total_count / data.count
                                var resto = data.total_count % data.count
                                if (resto > 0)
                                    div++
                                Log.e("TAG", "RESULTADO DE LA DIVISION $div ${pag < div}")
                                if (pag <= div) {
                                    val p = pag + 1
                                    consumeMon_c(p, p < div)
                                }
                            }else {
                                showMessage(getString(R.string.sync_exitosamente))
                                stopForeground(Service.STOP_FOREGROUND_REMOVE)
                            }


                        } catch (e: java.lang.Exception) {
                            stopForeground(Service.STOP_FOREGROUND_REMOVE)
                            showMessage(context.getString(R.string.error_api))

                        }

                    }


            }
        })
    }

    fun showMessage(message: String) { //Envia informacion a la VISTA (UI)
        val broadcastManager = LocalBroadcastManager.getInstance(this)

        val resultIntent = Intent(EVENT_COUNT_FINISHED)
        resultIntent.putExtra(EXTRA_SERVICE_STATUS, message)
        broadcastManager.sendBroadcast(resultIntent)
    }


}