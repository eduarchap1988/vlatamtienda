package com.vlatam.vlatamtienda.Servicios

import android.content.SharedPreferences
import com.google.gson.Gson
import com.vlatam.vlatamtienda.Data.Repository.DataLogin
import com.vlatam.vlatamtienda.Data.Repository.EmpM
import com.vlatam.vlatamtienda.Data.Local.EntM
import com.vlatam.vlatamtienda.Data.Repository.ResponseDepT
import com.vlatam.vlatamtienda.Data.Repository.UsrM

class PrefrencesManager constructor(preferences: SharedPreferences){
    private val preferences: SharedPreferences= preferences
    private val KEY_LOGININFO = "key_logininfo"
    private val KEY_BASEURL = "key_baseurl"
    private val KEY_ISLOGGED = "key_islogged"
    private val TRM = "key_trm"
    private val EMP = "key_emp"
    private val KEY_EMP_M = "key_emp_m"
    private val KEY_ENT_M= "key_ent_m"
    private val KEY_MOSTRAR_OCULTOS = "key_mostrar_ocultos"
    private val KEY_DEP_T = "key_dep_t"

    private val KEY_USR_M = "key_USR_M"

    fun setLoginInfo(dataLogin: DataLogin) {
        val editor = preferences.edit()
        editor.putString(KEY_LOGININFO, Gson().toJson(dataLogin))
        editor.apply()
    }


    fun getLoginInfo(): DataLogin? {
        val json_login = preferences.getString(KEY_LOGININFO, "")
        if (json_login.isEmpty())
            return null
        else
            return Gson().fromJson(json_login, DataLogin::class.java)
    }


    fun setBaseUrl(basepath: String) {
        val editor = preferences.edit()
        editor.putString(KEY_BASEURL, basepath)
        editor.apply()
    }

    fun setEmp(emp: String){
        val editor = preferences.edit()
        editor.putString(EMP, emp)
        editor.apply()
    }

    fun getEmp(): String{
        return preferences.getString(EMP, "")
    }

    fun setTrm(trm: String){
        val editor = preferences.edit()
        editor.putString(TRM, trm)
        editor.apply()
    }

    fun getTrm(): String{
        return preferences.getString(TRM, "")
    }


    fun getBasrUrl(): String {
        return preferences.getString(KEY_BASEURL, "")
    }

    fun setIsLogged() {
        val editor = preferences.edit()
        editor.putBoolean(KEY_ISLOGGED, true)
        editor.apply()
    }

    fun logout() {
        val editor = preferences.edit()
        editor.putBoolean(KEY_ISLOGGED, false)
        editor.apply()
    }



    fun isLogged(): Boolean {
        return preferences.getBoolean(KEY_ISLOGGED, false)
    }

    fun setMostrarOculto(mostrar: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(KEY_MOSTRAR_OCULTOS, mostrar)
        editor.apply()
    }

    fun getMostrarOculto(): Boolean {
        return preferences.getBoolean(KEY_MOSTRAR_OCULTOS, false)
    }

    fun setEmpM(empM: EmpM) {
        val editor = preferences.edit()
        editor.putString(KEY_EMP_M, Gson().toJson(empM))
        editor.apply()
    }

    fun getEmpM(): EmpM {
        return Gson().fromJson(preferences.getString(KEY_EMP_M, ""), EmpM::class.java)
    }


    fun setEntM(entM: EntM) {
        val editor = preferences.edit()
        editor.putString(KEY_ENT_M, Gson().toJson(entM))
        editor.apply()
    }

    fun getEntM(): EntM {
        return Gson().fromJson(preferences.getString(KEY_ENT_M, ""), EntM::class.java)
    }


    fun setDepT(depT: ResponseDepT.DepT) {
        val editor = preferences.edit()
        editor.putString(KEY_DEP_T, Gson().toJson(depT))
        editor.apply()
    }

    fun getDepT(): ResponseDepT.DepT {
        return Gson().fromJson(preferences.getString(KEY_DEP_T, ""), ResponseDepT.DepT::class.java)
    }



    fun setUserM(usr: UsrM) {
        val editor = preferences.edit()
        editor.putString(KEY_USR_M, Gson().toJson(usr))
        editor.apply()
    }

    fun getUsrM(): UsrM {
        return Gson().fromJson(preferences.getString(KEY_USR_M, ""), UsrM::class.java)
    }



}