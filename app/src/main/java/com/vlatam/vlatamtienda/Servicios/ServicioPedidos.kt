package com.vlatam.vlatamtienda.Servicios

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.preference.PreferenceManager
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.vlatam.vlatamtienda.Data.Local.*
import com.vlatam.vlatamtienda.Data.Repository.ResponseVtaPedG
import com.vlatam.vlatamtienda.Data.Repository.ResponseVtaPedLinG
import com.vlatam.vlatamtienda.Data.Repository.VtaPedG
import com.vlatam.vlatamtienda.R
import com.vlatam.vlatamtienda.VlatamTiendaApplication
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import com.vlatam.vlatamtienda.UI.MainActivity
import android.content.Context.NOTIFICATION_SERVICE
import android.graphics.Color
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationCompat.PRIORITY_MIN


class ServicioPedidos : Service()   {
    companion object {
        val CANONICAL_NAME = ServicioPedidos::class.java!!.getCanonicalName()
        val EXTRA_SERVICE_STATUS = "Status_servicio"
        val EVENT_COUNT_FINISHED = CANONICAL_NAME!! + ".COUNT_FINISHED"
    }

    private var lista_pedido: ListPedidos? = null
    private val client= OkHttpClient()
    private lateinit var pedido_selected: Pedido
    private val preferences= PrefrencesManager(PreferenceManager.getDefaultSharedPreferences(VlatamTiendaApplication.instance.getContext()))

    private var pedido_articulo_list: MutableList<PedidoArticulo> = arrayListOf()
    private var contador: Int=0

    private val daoPedidos = PedidoDao()
    private val daoArticulos= ArticulosDao()
    private val daoPedidoArticulo= PedidoArticuloDao()


    override fun onCreate() {
        Log.i("TAG", "Service onCreate")
        //showMessage(getString(R.string.sync_inicio))
    }

    private lateinit var notification: Notification

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        Log.i("TAG", "Service onStartCommand $startId")

        Log.e("TAG", "SERVICIO INICIADO")

        Toast.makeText(applicationContext, "El servicio de sincronizacion ha iniciado correctamente", Toast.LENGTH_LONG).show()
        if(intent==null){
            return Service.START_NOT_STICKY
        }

        if(intent!!.extras != null){
            Log.e("TAG", "data "+intent.getStringExtra("data"))
            lista_pedido = Gson().fromJson(intent.getStringExtra("data"), ListPedidos::class.java)
            if(!lista_pedido!!.pedidos.isEmpty())
                sincronizar(lista_pedido!!.pedidos[0])
        }


        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.action = Constants.ACTION.MAIN_ACTION
        notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)


        // startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);
        //NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        var channelId = ""

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel("service_pedido", "vLatamTienda")
        }

        val icon = BitmapFactory.decodeResource(resources, R.drawable.logo_vlatam_tienda)
        //Bitmap icon = BitmapFactory.decodeResource(getResources(), android.R.drawable.ic_dialog_map);

        notification = NotificationCompat.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setTicker(getString(R.string.app_name))
                .setContentText(getString(R.string.sincronizando_datos))
                .setSmallIcon(R.drawable.logo_circle)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setPriority(PRIORITY_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build()


        //notificationManager.notify(new Random().nextInt(), notification);
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification)

        return Service.START_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE)

        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    override fun onBind(intent: Intent): IBinder? {
        Log.i("TAG", "Service onBind")
        return null
    }

    override fun onDestroy() {
        Log.i("TAG", "Service onDestroy")
    }



    fun sincronizar(p: Pedido){
        pedido_selected= p

        pedido_articulo_list.clear()
        pedido_articulo_list= daoPedidoArticulo.queryForAll(pedido_selected.id!!)


        Log.e("TAG", "PEDIDO ACTUALIZADO "+pedido_selected)
        enviarCabecera()
    }




    private fun enviarCabecera() {


        var endpoint= preferences.getBasrUrl()+"v1/vta_ped_g?api_key="+ (preferences.getLoginInfo()?.api_key ?: "")

        var date= SimpleDateFormat("yyyy-MM-dd")
        var hora= SimpleDateFormat("HH:mm:ss")

        var fecha= SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(pedido_selected.hora)
        var param = JSONObject()
        param.put("id", 0)
        param.put("emp_div", preferences.getEmpM().id)
        //param.put("fch", date.format(Date(pedido_selected.hora))+"T"+hora.format(Date(pedido_selected.hora))+".000Z")
        param.put("fch", date.format(fecha))
        param.put("ser", preferences.getEmpM().ser_vta_ped)
        param.put("clt", pedido_selected.cliente!!.id)
        param.put("cmr", preferences.getUsrM().ent_rel) /***/
        param.put("alm", preferences.getUsrM().alm) /***/
        param.put("obs", pedido_selected.obs)
        param.put("nro_ord_com_clt", "") //vacio por ahora
        param.put("pre_con_iva_inc", true) //true siempre
        param.put("lat", pedido_selected.lat) //latitud
        param.put("lon", pedido_selected.lon) //longitud
        param.put("alt", pedido_selected.alt) //altura
        param.put("pre", pedido_selected.pre) //precision
        param.put("vel", pedido_selected.vel) // velocidad
        param.put("api_key", preferences.getLoginInfo()!!.api_key)

        if(pedido_selected.direccion!=null){
            param.put("cnd", pedido_selected.direccion!!.dir_etq)
        }else
            param.put("cnd", "")


        Log.e("TAG", "endpoint $endpoint")

        Log.e("TAG", "json en cabecera de factura "+param.toString())


        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                showMessage(applicationContext.getString(R.string.error_api))

            }

            @SuppressLint("NewApi")
            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "response en cabecera de factura $response")

                if(response.contains("El API Key de la solicitud no es válido")){
                    //pedido.postValue(Resource.error("El API Key de la solicitud no es válido", null))

                }else
                    if (response.isEmpty() || response == "{}") {
                       // pedido.postValue(Resource.error(context.getString(R.string.error_api), null))

                    } else {
                        try {
                            var data : ResponseVtaPedG = Gson().fromJson(response, ResponseVtaPedG::class.java)
                            if(data.count>0) {
                                pedido_selected.id_sincronizado= data.vta_ped_g[0].id
                                daoPedidos.update(pedido_selected)
                                var cont=1

                                Log.e("TAG", "LISTA DE ARTICULOS $pedido_articulo_list")

                                contador=0
                                pedido_articulo_list.forEach {
                                    it.art_m= daoArticulos.queryForId(it.art_m!!.id!!)

                                    enviarLinea(it, cont, data.vta_ped_g[0])
                                    cont++
                                }


                            }else{
                                showMessage(applicationContext.getString(R.string.error_api))
                                stopForeground(Service.STOP_FOREGROUND_REMOVE)
                            }


                        } catch (e: java.lang.Exception) {
                            //pedido.postValue(Resource.error(context.getString(R.string.error_api), null))
                            stopForeground(Service.STOP_FOREGROUND_REMOVE)
                        }



                    }


            }
        })




    }

    private fun enviarLinea(artM: PedidoArticulo, pos: Int, cabecera: VtaPedG){

        var endpoint= preferences.getBasrUrl()+"v1/vta_ped_lin_g?api_key="+ (preferences.getLoginInfo()?.api_key ?: "")

        var date= SimpleDateFormat("yyyy-MM-dd")
        var hora= SimpleDateFormat("HH:mm:ss")

        var fecha= SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(pedido_selected.hora)

        var param = JSONObject()
        param.put("id", 0)
        param.put("vta_ped", cabecera.id)
        param.put("num_lin", pos)
        //param.put("fch", date.format(Date(pedido_selected.hora)))
        //param.put("hor", date.format(Date(pedido_selected.hora))+"T"+hora.format(Date(pedido_selected.hora))+".000Z") // si elegimos un cliente sino va 0
        param.put("fch", date.format(fecha))
        param.put("hor", hora.format(fecha))
        param.put("emp", cabecera.emp_div) /***/
        param.put("clt", cabecera.clt) /***/
        param.put("ser", cabecera.ser)
        param.put("alm", cabecera.alm)
        param.put("art", artM.art_m!!.id)
        param.put("dsc", "")
        param.put("can_ped", artM.cantidad)
        param.put("can_ped_und", artM.cantidad)
        param.put("pre", artM.art_m!!.pvp)
        param.put("por_dto", 0)
        param.put("reg_iva_vta",  preferences.getEmpM().reg_iva_vta)
        param.put("und_med", artM.art_m!!.und_med_vta)

        Log.e("TAG", "endpoint $endpoint")

        Log.e("TAG", "json de linea de factura "+param.toString())


        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                showMessage(applicationContext.getString(R.string.error_api))
            }

            @SuppressLint("NewApi")
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                Log.e("TAG", "response en linea de factura $response")

                if(response.contains("El API Key de la solicitud no es válido")){
                   // pedido.postValue(Resource.error("El API Key de la solicitud no es válido", null))
                    showMessage(applicationContext.getString(R.string.error_api))

                }else
                    if (response.isEmpty() || response == "{}") {
                     //   pedido.postValue(Resource.error(context.getString(R.string.error_api), null))

                    } else {
                        try {
                            var data : ResponseVtaPedLinG = Gson().fromJson(response, ResponseVtaPedLinG::class.java)
                            if(data.count>=1) {
                                contador++

                                if(contador==pedido_articulo_list.size){
                                    Log.e("TAG", "contador igual a pedido ")
                                    pedido_selected.is_sincronizado=true
                                    pedido_selected.status= StatusPedido.SINCRONIZADO.name
                                    daoPedidos.update(pedido_selected)

                                    lista_pedido!!.pedidos.remove(pedido_selected)
                                    showMessage(getString(R.string.nuevo_pedido_enviado))

                                    if(!lista_pedido!!.pedidos.isEmpty())
                                        sincronizar(lista_pedido!!.pedidos[0])
                                    else {
                                        showMessage(getString(R.string.sync_exitosamente))
                                        stopForeground(Service.STOP_FOREGROUND_REMOVE)
                                    }
                                }


                            }else{
                                //pedido.postValue(Resource.error(context.getString(R.string.error_api), null))
                                showMessage(applicationContext.getString(R.string.error_api))
                                stopForeground(Service.STOP_FOREGROUND_REMOVE)
                            }

                        } catch (e: java.lang.Exception) {
                            //pedido.postValue(Resource.error(context.getString(R.string.error_api), null))
                            stopForeground(Service.STOP_FOREGROUND_REMOVE)
                        }



                    }


            }
        })



    }

    fun showMessage(message: String) { //Envia informacion a la VISTA (UI)
        val broadcastManager = LocalBroadcastManager.getInstance(this)

        val resultIntent = Intent(EVENT_COUNT_FINISHED)
        resultIntent.putExtra(EXTRA_SERVICE_STATUS, message)
        broadcastManager.sendBroadcast(resultIntent)
    }

    /*fun showMessage(message: String){
        Log.e("TAG", "MENSAJE $message")
       var mHandler = Handler()

        Thread(Runnable {
            mHandler.post {
                Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()

            }
        }).start()
    }*/


}