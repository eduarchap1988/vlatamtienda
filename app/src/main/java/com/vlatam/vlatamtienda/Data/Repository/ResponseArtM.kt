package com.vlatam.vlatamtienda.Data.Repository

import com.vlatam.vlatamtienda.Data.Local.ArtM

data class ResponseArtM(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val art_m: List<ArtM>
)