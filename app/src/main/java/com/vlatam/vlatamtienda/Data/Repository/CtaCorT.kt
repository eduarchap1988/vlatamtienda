package com.vlatam.vlatamtienda.Data.Repository

data class CtaCorT(
        var ent_m: Int,
        var mnt_cru: Double,
        var mnt_tot: Double,
        var mon_c: Int,
        var num_doc: String,
        var sal: Double,
        var tip_cta_cor_t: String,
        var fch: String,
        var fch_vto: String,

        var moneda: String= ""
)