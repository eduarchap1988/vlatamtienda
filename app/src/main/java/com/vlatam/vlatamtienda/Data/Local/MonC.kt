package com.vlatam.vlatamtienda.Data.Local

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatamtienda.Helper.DBHelper

@DatabaseTable(tableName = "mon_c")
data class MonC(
        @DatabaseField(generatedId = false, id = true)
        var id: Int? = null,
        @DatabaseField
        var iso: String= "",
        @DatabaseField
        var name: String= "",
        @DatabaseField
        var sim: String= ""
)


class MonedaDao{
    companion object {
        lateinit var dao: Dao<MonC, Int>
    }

    init {
        dao = DBHelper.getMonDao()
    }

    fun add(table: MonC) = dao.createOrUpdate(table)

    fun update(table: MonC) = dao.update(table)

    fun delete(table: MonC) = dao.delete(table)

    fun queryForAll() = dao.queryForAll()

    fun queryForId(id: Int) = dao.queryForId(id)


    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }
}
