package com.vlatam.vlatamtienda.Data.Repository


data class ResponseDepT(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val dep_t: List<DepT>
) {

    data class DepT(
            val id: Int,
            val emp: String,
            val emp_div: String,
            val name: String,
            val trm_tpv: Int
    )
}