package com.vlatam.vlatamtienda.Data.Local

data class PedidoSlected (
        var pedido: Pedido,
        var slected: Boolean
)