package com.vlatam.vlatamtienda.Data.Local

import com.google.gson.Gson
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatamtienda.Helper.DBHelper

@DatabaseTable(tableName = "vta_tar_cli_g")
data class VtaTarCliG(
        @DatabaseField(generatedId = false, id = true)
        var id: Int? = null,
        @DatabaseField
        var name: String= "",
        @DatabaseField
        var art: Int? = null,
        @DatabaseField
        var clt: Int? = null,
        @DatabaseField
        var pre: Float? = null,
        @DatabaseField
        var por_dto: Int? = null
){

    override fun toString(): String {
        return Gson().toJson(this)
    }
}

class TarifaClienteDao{
    companion object {
        lateinit var dao: Dao<VtaTarCliG, Int>
    }

    init {
        dao = DBHelper.getTarifaClienteDao()
    }

    fun add(table: VtaTarCliG) = dao.createOrUpdate(table)

    fun update(table: VtaTarCliG) = dao.update(table)

    fun delete(table: VtaTarCliG) = dao.delete(table)

    fun queryForAll() = dao.queryForAll()

    fun queryForId(id: Int) = dao.queryForId(id)

    fun queryTarifaCliente(cliente_id: Int, art_m: Int): VtaTarCliG?{
        var tarifa= dao.queryBuilder().where().eq("art", art_m).and().eq("clt", cliente_id).queryForFirst()
        return tarifa
    }


    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }
}
