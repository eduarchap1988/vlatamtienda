package com.vlatam.vlatamtienda.Data.Local

data class PedidoGroup (
        var pedido: MutableList<PedidoSlected>,
        var tittle: String,
        var slected: Boolean
){
    override fun equals(other: Any?): Boolean {
        if (other is PedidoGroup) {
            return other.tittle== this.tittle
        }else
            return false
    }
}