package com.vlatam.vlatamtienda.Data.Local

import com.google.gson.Gson
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatamtienda.Helper.DBHelper

@DatabaseTable(tableName = "vta_tar_g")
data class VtaTarG(
        @DatabaseField(generatedId = false, id = true)
        val id: Int? = null,
        @DatabaseField
        val name: String = ""
){
    override fun equals(other: Any?): Boolean {
        if (other is VtaTarG) {
            return other.id== this.id
        }else
            return false
    }
    override fun toString(): String {
        return Gson().toJson(this)
    }
}

class TarifasDao{
    companion object {
        lateinit var dao: Dao<VtaTarG, Int>
    }

    init {
        dao = DBHelper.getTarifasDao()
    }

    fun add(table: VtaTarG) = dao.createOrUpdate(table)

    fun update(table: VtaTarG) = dao.update(table)

    fun delete(table: VtaTarG) = dao.delete(table)

    fun queryForAll() = dao.queryForAll()

    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }
}
