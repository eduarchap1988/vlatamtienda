package com.vlatam.vlatamtienda.Data.Local

import com.google.gson.Gson
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatamtienda.Helper.DBHelper

@DatabaseTable(tableName = "ent_m")
data class EntM(
        @DatabaseField(generatedId = false, id = true)
        val id: Int? = null,
        @DatabaseField
        val nom_fis: String = "",
        @DatabaseField
        val nom_com: String = "",
        @DatabaseField
        val cif: String = "",
        @DatabaseField
        val img: String = "",
        @DatabaseField
        val tlf: String = "",
        @DatabaseField
        val eml: String = "",
        @DatabaseField
        val dir: String = "",
        @DatabaseField
        var vta_tar: Int = 1,
        @DatabaseField
        val cmr: Int? = null
){

    override fun toString(): String {
        return Gson().toJson(this)
    }
}

class ClienteDao{
    companion object {
        lateinit var dao: Dao<EntM, Int>
    }

    init {
        dao = DBHelper.getClienteDao()
    }

    fun add(table: EntM) = dao.createOrUpdate(table)

    fun update(table: EntM) = dao.update(table)

    fun delete(table: EntM) = dao.delete(table)

    fun queryForAll() = dao.queryForAll()

    fun queryForId(id: Int) = dao.queryForId(id)

    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }
}
