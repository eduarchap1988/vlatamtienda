package com.vlatam.vlatamtienda.Data.Local

import com.google.gson.Gson
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatamtienda.Helper.DBHelper

@DatabaseTable(tableName = "pedido_articulo")
data class PedidoArticulo(
        @DatabaseField(generatedId = true)
        var id: Int? = null,

        @DatabaseField(foreign = true, columnName = "pedido_id")
        var pedido: Pedido?= null,

        @DatabaseField(foreign = true, columnName = "artm_id")
        var art_m: ArtM?= null,

        @DatabaseField
        var cantidad: Float = 1f,

        @DatabaseField
        var isEditable: Boolean= true
){
    override fun toString(): String {
        return Gson().toJson(this)
    }

    override fun equals(other: Any?): Boolean {
        if (other is PedidoArticulo) {
            return other.art_m!!.id== this.art_m!!.id
        }else
            return false
    }
}


class PedidoArticuloDao{
    companion object {
        lateinit var dao: Dao<PedidoArticulo, Int>
    }

    init {
        dao = DBHelper.getPedidoArticuloDao()
    }

    fun add(table: PedidoArticulo) = dao.createOrUpdate(table)

    fun update(table: PedidoArticulo) = dao.update(table)

    fun delete(table: PedidoArticulo) = dao.delete(table)

    fun queryForAll(id_pedido: Int) =
            dao.queryBuilder().where().eq("pedido_id", id_pedido).query()

    fun queryForId(id: Int) =
            dao.queryBuilder().where().eq("id", id).queryForFirst()

    fun removeAll(id_pedido: Int) {
        for (table in queryForAll(id_pedido)) {
            dao.delete(table)
        }
    }

    fun queryForAll()= dao.queryForAll()

    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }
}