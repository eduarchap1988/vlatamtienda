package com.vlatam.vlatamtienda.Data.Local

import com.google.gson.Gson
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatamtienda.Helper.DBHelper


@DatabaseTable(tableName = "art_m")
data class ArtM(
        @DatabaseField(generatedId = false, id = true)
        val id: Int? = null,
        @DatabaseField
        val name: String = "",
        @DatabaseField
        val img: String = "",
        @DatabaseField
        val ref: String = "",
        @DatabaseField
        val reg_iva_vta: String = "",
        @DatabaseField
        val exs: Float? = null,
        @DatabaseField
        val tpv_vis: Boolean? = null,
        @DatabaseField
        var pvp: Float? = null,
        @DatabaseField
        val und_med_vta: Int? = null

){


    override fun toString(): String {
        return Gson().toJson(this)
    }
}


class ArticulosDao{
    companion object {
        lateinit var dao: Dao<ArtM, Int>
    }

    init {
        dao = DBHelper.getArticulosDao()
    }

    fun add(table: ArtM) = dao.createOrUpdate(table)

    fun update(table: ArtM) = dao.update(table)

    fun delete(table: ArtM) = dao.delete(table)

    fun queryForAll() = dao.queryForAll()

    fun queryForId(id: Int) = dao.queryForId(id)


    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }
}
