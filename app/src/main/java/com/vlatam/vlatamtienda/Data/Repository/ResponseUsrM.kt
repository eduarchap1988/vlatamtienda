package com.vlatam.vlatamtienda.Data.Repository

data class ResponseUsrM(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val usr_m: List<UsrM>
)