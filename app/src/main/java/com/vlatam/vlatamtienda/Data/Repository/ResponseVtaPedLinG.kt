package com.vlatam.vlatamtienda.Data.Repository

data class ResponseVtaPedLinG(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val vta_ped_lin_g: List<VtaPedLinG>
)