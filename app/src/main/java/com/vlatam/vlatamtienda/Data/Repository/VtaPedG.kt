package com.vlatam.vlatamtienda.Data.Repository

data class VtaPedG(
        val id: Int,
        val emp_div: String,
        val fch: String,
        val ser: Int,
        val clt: Int,
        val cmr: Int,
        val alm: String,
        val obs: String,
        val nro_ord_com_clt: String,
        val pre_con_iva_inc: Boolean,
        val lat: Double,
        val lon: Double,
        val alt: Double,
        val pre: Float,
        val vel: Float,
        val api_key: String
)