package com.vlatam.vlatamtienda.Data.Repository

import com.vlatam.vlatamtienda.Data.Local.EntM

data class ResponseEntM(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val ent_m: List<EntM>
)