package com.vlatam.vlatamtienda.Data.Repository

import com.vlatam.vlatamtienda.Data.Local.VtaTarG

data class ResponseVtaTarG(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val vta_tar_g: List<VtaTarG>
)