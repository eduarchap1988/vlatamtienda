package com.vlatam.vlatamtienda.Data.Repository

import com.vlatam.vlatamtienda.Data.Local.MonC

data class ResponseMonC(
        var api_key: String,
        var count: Int,
        var mon_c: List<MonC>,
        var swagger: String,
        var total_count: Int
)