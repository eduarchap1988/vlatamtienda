package com.vlatam.vlatamtienda.Data.Repository

import com.vlatam.vlatamtienda.Data.Local.VtaTarArtG

data class ResponseVtaTarArtG(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val vta_tar_art_g: List<VtaTarArtG>
)