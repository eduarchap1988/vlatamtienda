package com.vlatam.vlatamtienda.Data.Local

data class ClienteDireccion (
        var cliente: EntM,
        var direccion: DirM?,
        var vta_tar_g: List<VtaTarG> = arrayListOf()
)