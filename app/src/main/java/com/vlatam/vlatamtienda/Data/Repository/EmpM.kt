package com.vlatam.vlatamtienda.Data.Repository

data class EmpM(
        val id: String,
        val name: String,
        val ent: Int,
        val por_iva_gen: Float,
        val por_iva_red: Float,
        val por_iva_sup: Float,
        val por_iva_esp: Float,
        val reg_iva_vta: String,
        val mon_sim: String,
        val ser_vta_ped: Int
)