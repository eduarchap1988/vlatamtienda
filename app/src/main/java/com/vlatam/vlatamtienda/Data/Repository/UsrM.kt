package com.vlatam.vlatamtienda.Data.Repository

data class UsrM(
        val id: Int,
        val name: String,
        val img: String,
        val ent_rel: Int,
        val alm: String,
        val fpg: Int,
        val ser: Int
)