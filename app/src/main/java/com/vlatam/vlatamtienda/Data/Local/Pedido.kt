package com.vlatam.vlatamtienda.Data.Local

import android.util.Log
import com.google.gson.Gson
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatamtienda.Helper.DBHelper
import java.text.SimpleDateFormat
import java.util.*


@DatabaseTable(tableName = "pedido")
data class Pedido(

        @DatabaseField(generatedId = true)
        var id: Int? = null,

        @DatabaseField
        var total: Float = 0f,

        @DatabaseField
        var total_articulos: Float = 0f,

        @DatabaseField(foreign = true, columnName = "cliente_id")
        var cliente: EntM?= null,

        @DatabaseField(foreign = true, columnName = "dirm_id")
        var direccion: DirM?= null,

        @DatabaseField
        var status: String = StatusPedido.PENDIENTE.name,

        @DatabaseField
        var hora: String = SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Date()),

        @DatabaseField
        var is_sincronizado: Boolean= false,

        @DatabaseField
        var id_sincronizado: Int= 0,

        @DatabaseField
        var is_editable: Boolean= true,

        @DatabaseField
        var mon_sim: String = "",  //simbolo de la moneda

        @DatabaseField
        var obs: String = "",//observacion

        @DatabaseField
        var lat: Double = 0.0,

        @DatabaseField
        var lon: Double = 0.0,

        @DatabaseField
        var alt: Double = 0.0,

        @DatabaseField
        var pre: Float = 0f,

        @DatabaseField
        var vel: Float = 0f,

        @DatabaseField
        var vta_tar: Int = 1

){
    override fun equals(other: Any?): Boolean {
        if (other is Pedido) {
            return other.id== this.id
        }else
        return false
    }

    override fun toString(): String {
        return Gson().toJson(this)
    }
}

class PedidoDao{
    companion object {
        public lateinit var dao: Dao<Pedido, Int>
    }

    init {
        dao = DBHelper.getPedidoDao()
    }

    fun add(table: Pedido) = dao.createOrUpdate(table)

    fun update(table: Pedido) = dao.update(table)

    fun delete(table: Pedido) = dao.delete(table)

    fun queryForAll()= dao.queryForAll()

    fun queryForAllPendientes()=  dao.queryBuilder().where().eq("is_sincronizado", false).query()

    fun queryForAllSincronizados()=  dao.queryBuilder().where().eq("is_sincronizado", true).query()

    fun getUltimoPedido(): Pedido{

        var max= dao.queryRawValue("select max(id) from pedido")

        return dao.queryBuilder().where().eq("id", max).queryForFirst()
    }

    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }
}