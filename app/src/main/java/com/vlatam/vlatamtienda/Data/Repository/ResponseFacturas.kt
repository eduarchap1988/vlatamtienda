package com.vlatam.vlatamtienda.Data.Repository

data class ResponseFacturas(
        var api_key: String,
        var count: Int,
        var cta_cor_t: List<CtaCorT>,
        var swagger: String,
        var total_count: Int
)