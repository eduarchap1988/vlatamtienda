package com.vlatam.vlatamtienda.Data.Local

data class ClienteOpcion (
        var cliente: EntM,
        var opcion: Int
)