package com.vlatam.vlatamtienda.Data.Repository

import com.vlatam.vlatamtienda.Data.Local.DirM

data class ResponseDirM(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val dir_m: List<DirM>
)