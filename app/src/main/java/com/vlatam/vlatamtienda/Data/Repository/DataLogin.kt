package com.vlatam.vlatamtienda.Data.Repository

import com.google.gson.Gson


data class DataLogin(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val api_key_w: List<ApiKeyW>
) {

    data class ApiKeyW(
            val id: Int,
            val name: String,
            val api_key: String,
            val usr: Int
    )


    {
        override fun toString(): String {
            return Gson().toJson(this)
        }
    }
}