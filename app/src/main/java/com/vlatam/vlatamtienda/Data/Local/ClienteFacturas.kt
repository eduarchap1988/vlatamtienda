package com.vlatam.vlatamtienda.Data.Local

import com.vlatam.vlatamtienda.Data.Repository.CtaCorT

data class ClienteFacturas (
        var cliente: EntM,
        var facturas: List<CtaCorT>
)