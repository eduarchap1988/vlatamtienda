package com.vlatam.vlatamtienda.Data.Repository

data class VtaPedLinG(
        val id: Int,
        val vta_ped: Int,
        val num_lin: Int,
        val fch: String,
        val hor: String,
        val emp: String,
        val clt: Int,
        val ser: Int,
        val alm: String,
        val art: Int,
        val dsc: String,
        val can_ped: Double,
        val pre: Double,
        val por_dto: Double,
        val reg_iva_vta: String,
        val und_med: Int
)