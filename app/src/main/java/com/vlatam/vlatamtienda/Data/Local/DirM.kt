package com.vlatam.vlatamtienda.Data.Local

import com.google.gson.Gson
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatamtienda.Helper.DBHelper


@DatabaseTable(tableName = "dir_m")
data class DirM(
        @DatabaseField(generatedId = false, id = true)
        val id: Int? = null,
        @DatabaseField
        val ent: Int? = null,
        @DatabaseField
        val obs: String = "",
        @DatabaseField
        val dir: String = "",
        @DatabaseField
        val dir_2: String = "",
        @DatabaseField
        val cps: String = "",
        @DatabaseField
        val loc: String = "",
        @DatabaseField
        val dir_com: String = "",
        @DatabaseField
        val dir_etq: String = "",
        @DatabaseField
        val cal: String = "",
        @DatabaseField
        val nro: String = ""
){
    override fun toString(): String {
        return Gson().toJson(this)
    }
}


class DirectorioDao{
    companion object {
        lateinit var dao: Dao<DirM, Int>
    }

    init {
        dao = DBHelper.getDirectorioDao()
    }

    fun add(table: DirM) = dao.createOrUpdate(table)

    fun update(table: DirM) = dao.update(table)

    fun delete(table: DirM) = dao.delete(table)

    fun queryForAll() =
            dao.queryForAll()

    fun queryForAll(ent: Int) = dao.queryBuilder().where().eq("ent", ent).query()

    fun queryForId(dir: Int) = dao.queryBuilder().where().eq("id", dir).queryForFirst()

    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }
}
