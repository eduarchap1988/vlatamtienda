package com.vlatam.vlatamtienda.Data.Local

enum class StatusPedido {
    PENDIENTE,
    SINCRONIZADO
}