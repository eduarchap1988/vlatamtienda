package com.vlatam.vlatamtienda.Data.Repository

import com.vlatam.vlatamtienda.Data.Local.VtaTarCliG

data class ResponseVtaTarCliG(
        var count: Int,
        var total_count: Int,
        var api_key: String,
        var swagger: String,
        var vta_tar_cli_g: List<VtaTarCliG>
)